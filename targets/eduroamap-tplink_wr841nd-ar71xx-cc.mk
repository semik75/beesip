TARGET_CPU=ar71xx
OWRT_NAME=chaos_calmer

# HWREV can be added as commandline argument to make
ifeq ($(HWREV),)
HWREV=wr841n-v9
endif

TARGET_NAME=eduroamap_$(HWREV)_$(BEESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=mips
TARGET_QEMU_OPTS=-m64

$(eval $(call BeesipDefaults,eduroam))

BEESIP_PACKAGES += \
	lldpd=n tcpdump=n tcpdump-mini=n dnsmasq=n tftp-hpa=n \
	ip=n odhcp6c=n odhcpd=n kmod-ipv6=n \
	wpad=y hostapd=n firewall=n syslog-ng3=n openvpn-openssl=n \
	openssl-util=n iftop=n scdp=n eapol-test=n flow-tools=n \
	iptables=n ip6tables=n firewall=n luci-app-mwan3=n \
	luci-app-firewall=n luci=n freifunk-firewall=n \
	freifunk-gwcheck=n luci-mod-freifunk=n luci-app-p2pblock=n \
	luci-app-multiwan=n meshwizard=n luci-mod-freifunk-community=n

OWRT_CONFIG_UNSET += \
	PACKAGE_firewall PACKAGE_luci-app-mwan3 PACKAGE_luci-app-firewall \
	PACKAGE_luci PACKAGE_freifunk-firewall PACKAGE_freifunk-gwcheck \
	PACKAGE_luci-mod-freifunk PACKAGE_luci-app-p2pblock PACKAGE_luci-app-multiwan \
	PACKAGE_meshwizard PACKAGE_luci-mod-freifunk-community \
	DEFAULT_dnsmasq DEFAULT_iptables DEFAULT_ip6tables DEFAULT_odhcp6c DEFAULT_odhcpd \
        DEFAULT_wpad-mini

OWRT_IMG_BIN_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-generic-tl-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_SYSUPGRADE_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-generic-tl-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_FACTORY_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-generic-tl-$(HWREV)-squashfs-factory.bin
OWRT_IMG_PROFILE=TLWR841
OWRT_IMG_KERNEL_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-generic-vmlinux.elf

OWRT_CONFIG_SET += TARGET_ar71xx=y TARGET_ar71xx_generic_TLWR841=y
