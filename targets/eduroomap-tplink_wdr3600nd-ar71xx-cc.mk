TARGET_CPU=ar71xx
OWRT_NAME=chaos_calmer
BEESIP_NAME=chaos_calmer

TARGET_NAME=eduroomap_$(HWREV)_$(BEESIP_VERSION)-owrt_$(OWRT_NAME)

HWREV=wdr3600-v1

TARGET_QEMU=mips
TARGET_QEMU_OPTS=-m64

$(eval $(call BeesipDefaults,eduroam))
$(eval $(call BeesipDefaults,eduroom))

OWRT_IMG_BIN_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_SYSUPGRADE_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_FACTORY_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-factory.bin
OWRT_IMG_PROFILE=TLWDR4300
OWRT_IMG_KERNEL_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-generic-vmlinux.elf

OWRT_CONFIG_SET +=  TARGET_ar71xx=y TARGET_ar71xx_generic_TLWDR4300=y


