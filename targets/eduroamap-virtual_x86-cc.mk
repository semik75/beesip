$(eval $(call BeesipDefaults,x86))
$(eval $(call BeesipDefaults,virtual))
$(eval $(call BeesipDefaults,eduroam))

# Target for virtual eduroom x86 host

# Target CPU - mandatory settings
TARGET_CPU=x86

# OpenWrt used for compilation (trunk, attitude_adjustment, ...)
OWRT_NAME=chaos_calmer

# Will be used ad prefix for various images
TARGET_NAME=virtual_eduroam-$(TARGET_CPU)-owrt_$(OWRT_NAME)

# Options for qemu emulation of target
TARGET_QEMU=i386
TARGET_QEMU_OPTS=-m 512

# ImageBuilder profile for your platform
OWRT_IMG_PROFILE=Generic

# More packages to include into image
BEESIP_PACKAGES +=

# Which config settings of OpenWrt should be set to .config
OWRT_CONFIG_SET += TARGET_x86=y HAS_SUBTARGETS=y TARGET_BOARD=\"x86\"

# Which files to manualy add to filesystem just during build (from:to)
OWRT_ADDED_FILES += virtual-files/network.uci:/etc/config/network virtual-files/dhcp.uci:/etc/config/dhcp
