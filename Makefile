.SUFFIXES:

include inc/config.mk
include inc/lib.mk
include inc/version.inc

# We can define CFG in local.mk
-include inc/local.mk

ifneq ($(TARGET),)
  CFG=targets/$(TARGET).mk
endif

# If CFG was specified, include CFG file, else x86-beesip.mk
ifneq ($(CFG),)
-include $(CFG)
else
include targets/virtual-x86-trunk.mk
endif

# Load default files to copy
$(eval $(call BeesipDefaults,files))

include inc/postconfig.mk

# Include again to respect new variables
include inc/lib.mk

# Base procedures
################################################

all: info prepare config build

clean: clean-build-dir

prepare: update-cleanstamp unpatch-owrt download-owrt download-external-files update-feeds patch-owrt

config: force-config-owrt

build: prereq prepare-kernel owrt-image beesip-images

################################################

quick: info beesip-packages
	make -C $(OWRT_DIR) target/linux/install

info:
	@printf "\n============== Building BEESIP $(TARGET_NAME) for architecture $(TARGET_CPU)\n"
	@echo "Beesip revision: $(BEESIP_GIT_REV)"
	@echo "Beesip verion: $(BEESIP_VERSION)"
	@echo "OpenWrt name: $(OWRT_NAME)"
	@echo "OpenWrt revision: $(OWRT_GIT_REV)"
	@echo "OpenWrt build dir: $(OWRT_DIR)"
	@echo "OpenWrt force revision: $(OWRT_FORCE_GIT_REV)"
	@echo "OpenWrt force packages revision: $(OWRT_FORCE_PACKAGES_GIT_REV)"
	@echo "OpenWrt GIT url: $(OWRT_GIT)"
	@echo 'OpenWrt feeds dir: $(OWRT_FEEDS_PKG),$(OWRT_FEEDS_LUCI),$(OWRT_FEEDS_ROUTING),$(OWRT_FEEDS_TELEPHONY)'
	@echo "Openwrt images: $(OWRT_IMAGES)"
	@echo "Target images will be: $(BEESIP_IMAGES)"
	@echo "Default settings: $(BEESIP_DEFAULTS)"
	@echo "No default settings: $(BEESIP_NODEFAULTS)"
ifneq ($(VERBOSE),)
	@echo "OpenWrt packages: '$(BEESIP_PACKAGES)'"
	@echo "OpenWrt added files: '$(OWRT_ADDED_FILES)'"
endif
	@printf "==============\n\n\n"

info2:  info
	@echo "Linux dir: $$($(OWRT_DIR)/scripts/shell.sh env LINUX_DIR)"
	@echo "Linux config: $$($(OWRT_DIR)/scripts/shell.sh env LINUX_DIR)/.config"
	@echo "Linux target config: $$($(OWRT_DIR)/scripts/shell.sh env LINUX_TARGET_CONFIG)"
	@echo "Target dir: $(BEESIP_IMG_DIR)"
	@echo "Log directory: $(LOG_DIR)"
	@echo "Stamp directory: $(STAMP_DIR)"
	@echo "Make arguments: $(MAKE_ARGS)"
	@echo "OpenWrt config set: '$(OWRT_CONFIG_SET)'"
	@echo "OpenWrt config unset: '$(OWRT_CONFIG_UNSET)'"
	@echo "Embedded modules: '$(EMBEDDED_MODULES)'"
	@echo "Dependencies: $(BEESIP_DEPENDS)"
	@printf "==============\n\n\n"

env-info:
	@echo "BEESIP_GIT_REV=$(BEESIP_GIT_REV)"
	@echo "BEESIP_VERSION=$(BEESIP_VERSION)"
	@echo "OWRT_DIR=$(OWRT_DIR)"
	@echo "BEESIP_IMG_DIR=$(BEESIP_IMG_DIR)"
	@echo "MAKE=$(MAKE) CFG=$(CFG)"
	@echo "OMAKE=$(BMAKE) -C $(OWRT_DIR)"
	@echo "OWRT_ADDED_FILES='$(OWRT_ADDED_FILES)'"

prereq: $(SPREREQ)
$(SPREREQ):
	$(call mkdirall)
	if [ -f $(OWRT_DIR)/.config ]; then \
	  $(BMAKE) -C $(OWRT_DIR) prereq; \
	  touch $(SPREREQ); \
	fi

download-owrt: git-clone-owrt
git-clone-owrt: $(SGITCLONE)
$(SGITCLONE): $(SPREREQ)
	if ! [ -f $(OWRT_DIR)/Makefile ]; then mkdir -p $(OWRT_DIR) && cd $(OWRT_DIR) && git init . && git remote add -t \* -f origin $(OWRT_GIT) && git checkout master; fi
	cd $(OWRT_DIR) && git pull ; git checkout $(OWRT_FORCE_GIT_REV) .
	$(call mkdirall)
	touch $(SGITCLONE)

download-external-files:
	if [ -d $(OWRT_DIR)/external-files ]; then rm -rf external-files; fi
  ifneq ($(EXTERNAL_FILES_REPO),)
		git clone $(EXTERNAL_FILES_REPO) external-files
  else
		@echo "External files repo not defined, I am not downloading it."
  endif

update-feeds: $(SFEEDSU)
$(SFEEDSU): download-owrt
	$(eval $(call AddOpenWrtFeed,src-link beesip $(PWD)/packages))
	$(eval $(call AddOpenWrtFeed,src-link beesip_$(OWRT_NAME) $(PWD)/packages-$(OWRT_NAME)))
  ifeq ($(BEESIP_VERSION),trunk)
	$(eval $(call AddOpenWrtFeed,src-link beesip_incomplete $(PWD)/packages-incomplete))
  endif
  ifeq ($(USE_CUSTOM_FEEDS), 1)
	printf "%b" $(OWRT_DEFAULT_FEEDS) > $(OWRT_DIR)/feeds.conf
	echo "" >> $(OWRT_DIR)/feeds.conf
  else
	cp $(OWRT_DIR)/feeds.conf.default $(OWRT_DIR)/feeds.conf
  endif
	printf "%b" $(ADDED_FEEDS) >> $(OWRT_DIR)/feeds.conf
	echo "" >> $(OWRT_DIR)/feeds.conf
	cd $(OWRT_DIR); ./scripts/feeds update -a
	touch $(SFEEDSU)

clean-build-dir:
	$(RM) -rf $(PWD)/external-files
	$(RM) -rf $(OWRT_DIR) $(STAMP_DIR)

soft-clean: clean-beesip clean-owrt gitclean clean-kvm
	@if [ -n "$(BEESIP_IMG_DIR)" ]; then $(RM) -rf $(BEESIP_IMG_DIR); fi
	@if [ -n "$(BEESIP_PACKAGES_DIR)" ];then $(RM) -rf $(BEESIP_PACKAGES_DIR); fi
	@if [ -n "$(LOG_DIR)" ];then $(RM) -rf $(LOG_DIR); fi
	@if [ -d "$(OWRT_DIR)" ]; then $(BMAKE) -C $(OWRT_DIR) -j$(JOBS) defconfig package/cleanup package/base-files/clean; fi
	$(RM) -f $(SOWRTCFG)

clean-owrt:
	@make -C $(OWRT_DIR) clean -j4

clean-stamp:
	$(RM) -f $(STAMP_DIR)/* $(OSTAMP_DIR)/*; true

clean-image:
	$(RM) -f $(BEESIP_IMG_DIR)/* $(OWRT_IMG) $(OWRT_IMG_SQUASHFS) $(SOWRTIMG); true

clean-feeds:
	cd $(OWRT_DIR); ./scripts/feeds clean
	$(RM) -f $(SFEEDSU) $(OWRT_DIR)/feeds.conf $(OWRT_DIR)/feeds/beesip

clean-beesip:
	if [ -d $(OWRT_DIR)/feeds/beesip/net/beesip ]; then $(BMAKE) -C $(OWRT_DIR)/feeds/beesip/net/beesip clean TOPDIR=$(OWRT_DIR); fi
	$(RM) -f $(SOWRTIMG) $(BEESIP_IMAGES)

distclean:
	$(RM) -rf $(OWRT_DIR) $(BUILD_DIR)/* $(BUILD_DIR)/dl $(STAMP_DIR)/*

dirclean:
	@$(BMAKE) -C $(OWRT_DIR) defconfig dirclean -j$(JOBS)
	@$(BMAKE) gitclean

gitclean:
	if [ -d "$(OWRT_DIR)" ]; then \
	  cd $(OWRT_DIR) && git reset --hard FETCH_HEAD && git clean -f -d; \
	 for i in $(OWRT_DIR)/feeds/*; do \
	   if [ -d $$i/.svn ]; then svn revert -R $$i; fi; \
	   if [ -d $$i/.git ]; then (cd $$i; git reset --hard FETCH_HEAD && git clean -f -d ); fi; \
	 done; \
	 $(RM) -f $(OSTAMP_DIR)/*; \
	fi

update: update-cleanstamp unpatch-owrt download-owrt update-feeds patch-owrt config-owrt

update-cleanstamp:
	$(RM) -f $(SFEEDSU) $(SFEEDSI) update-owrt $(SOWRTCFG)

changes:
	git status >&2
	git diff --no-prefix

owrt-changes:
	cd $(OWRT_DIR) && git status >&2 &&	git diff --no-prefix
	cd $(OWRT_DIR) && for i in feeds/*; do \
		[ -d $$i/.git  ] && (cd $$i && git status >&2 && git diff --no-prefix); \
	done

force-install-feeds:
	$(RM) -f $(SFEEDSI)
	@$(BMAKE) install-feeds

install-feeds: $(SFEEDSI)
$(SFEEDSI):
	cd $(OWRT_DIR); ./scripts/feeds install -a
	touch $(SFEEDSI)
	$(RM) -f $(SOWRTCFG)

uninstall-feeds:
	cd $(OWRT_DIR); ./scripts/feeds uninstall -a
	$(RM) -f $(SFEEDSI) $(SOWRTCFG)

update-owrt: download-owrt
$(SGITPULL):
	cd $(OWRT_DIR) && git pull ; git checkout $(OWRT_FORCE_GIT_REV) .
	touch $(SGITPULL)
	$(RM) -f $(SOWRTCFG)

test-dotconf:
	if ! [ -f "$(OWRT_DIR)/.config" ]; then $(RM) -f $(SOWRTCFG); fi

config-owrt: test-dotconf $(SOWRTCFG)
$(SOWRTCFG):
	@$(BMAKE) install-feeds
	@cd $(OWRT_DIR); \
	if [ -n "$(TARGET_CFG)" ]; then \
		grep -v -E '$(shell echo $(COMPILE_PACKAGES) $(BEESIP_PACKAGES) $(OWRT_CONFIG_SET)|sed "s/\(.*\)\=/\1\=/g")\>\|some_nunusual_text' $(TARGET_CFG) >$(OWRT_DIR)/.config-tmp1; \
	else \
		$(RM) -f $(OWRT_DIR)/.config; \
		touch $(OWRT_DIR)/.config-tmp1; \
	fi
	@[ -n "$(VERBOSE)" ] && printf "ENABLING packages: "; true
	@for i in $(COMPILE_PACKAGES) $(BEESIP_PACKAGES); do \
		[ -n "$(VERBOSE)" ] && echo -n "CONFIG_PACKAGE_$$i, " >&2; \
		echo CONFIG_PACKAGE_$$i ; \
	done <$(OWRT_DIR)/.config-tmp1 >$(OWRT_DIR)/.config-tmp2;
ifneq ($(BEESIP_DEPS),)
	@(for i in $(BEESIP_DEPS); do echo CONFIG_PACKAGE_$$(echo $$i |tr -d '+')=$(BEESIPPKG) ; done | grep -v CONFIG_PACKAGE_beesip) >>$(OWRT_DIR)/.config-tmp2
endif
	@[ -n "$(VERBOSE)" ] && printf "ENABLING configs: "; true
	@for i in $(OWRT_CONFIG_SET); do \
		[ -n "$(VERBOSE)" ] && printf "CONFIG_$$i, " >&2; \
		echo CONFIG_$$i; \
	done >>$(OWRT_DIR)/.config-tmp2;
	@$(CP) $(OWRT_DIR)/.config-tmp2 $(OWRT_DIR)/.config
	@$(BMAKE) -C $(OWRT_DIR) defconfig >/dev/null
	@cat $(OWRT_DIR)/.config | grep -vE '$(shell echo $(OWRT_CONFIG_UNSET)|sed "s/\ /\(\>\|\=\|\-\)\|/g")|some_unusual_text' >$(OWRT_DIR)/.config-tmp3; true
	@for i in $(OWRT_CONFIG_UNSET); do echo "# CONFIG_$$i is not set"; done >>$(OWRT_DIR)/.config-tmp3;
	@$(CP)  $(OWRT_DIR)/.config-tmp3  $(OWRT_DIR)/.config
	@for i in $(filter %=y,$(BEESIP_PACKAGES)); do if ! grep -q $$i $(OWRT_DIR)/.config; then echo Missing package $$i, look into feeds and dependencies; exit 2; fi; done
	@for i in $(filter %=m,$(BEESIP_PACKAGES)); do if ! grep -q $$i $(OWRT_DIR)/.config; then echo Missing package $$i, look into feeds and dependencies; exit 2; fi; done
	@for i in $(filter %=n,$(BEESIP_PACKAGES)); do pkg="$$(echo $$i | cut -d '=' -f 1)"; if ! grep -q "^#.*PACKAGE_$$pkg " $(OWRT_DIR)/.config && grep -q "PACKAGE_$$pkg" $(OWRT_DIR)/.config; then echo Package $$i should be disabled. See depends; \
	   awk '/^Package:/ {  printf "%s",""$$2" " } /^Depends:/ { print }' $(OWRT_DIR)/tmp/.packageinfo | grep -w $$pkg; \
	   echo "You should change BEESIP_CONFIG_UNSET: "; awk '/^Package:/ {  printf "%s",""$$2" " } /^Depends:/ { print }' $(OWRT_DIR)/tmp/.packageinfo | grep -w $$pkg | while read pkg rest; do printf "PACKAGE_$$pkg "; done; echo; \
	   echo "You should change BEESIP_PACKAGES: "; awk '/^Package:/ {  printf "%s",""$$2" " } /^Depends:/ { print }' $(OWRT_DIR)/tmp/.packageinfo | grep -w $$pkg | while read pkg rest; do printf "$$pkg=n "; done; echo; \
	   exit 2; \
	fi; done
	@for i in $(OWRT_CONFIG_SET); do echo $$i; done | sort | uniq -d | while read line; do echo Duplicate config set $$line; exit 2; done
	@for i in $(OWRT_CONFIG_SET); do if ! grep -q $$i $(OWRT_DIR)/.config; then echo Missing config option $$i, problem with config; exit 2; fi; done
	@for i in $(OWRT_CONFIG_UNSET); do if grep -q $$i $(OWRT_DIR)/.config | grep -qv '^#'; then echo Disabled package $$i inside! Problem with config.; exit 2; fi; done
	@if [ -d $(OWRT_DIR)/files/ ]; then $(RM) -rf $(OWRT_DIR)/files; fi
	@if [ -d $(OWRT_DIR)/external-files/ ]; then $(RM) -rf $(OWRT_DIR)/external-files; fi
	@$(call AddExternalFiles)
	@$(call AddFiles)
	@mkdir -p $(OWRT_DIR)/files/etc/beesip/defaults
	@$(call AddUciprovOpkg,$(UCIPROV_OPKG)) >$(OWRT_DIR)/files/etc/beesip/defaults/opkg.fd
	@$(call AddUciprovDefaults,$(UCIPROV_DEFAULTS)) >$(OWRT_DIR)/files/etc/beesip/defaults/uci.fd
	@$(call AddUciprovServices,$(UCIPROV_SERVICES)) >$(OWRT_DIR)/files/etc/beesip/defaults/services.fd
	@echo OWRT_IMG_SYSUPGRADE_NAME=$(OWRT_IMG_SYSUPGRADE_NAME) >$(OWRT_DIR)/tmp/beesip-inc.mk
	@echo BEESIP_VERSION=$(BEESIP_VERSION) > $(OWRT_DIR)/files/etc/beesip_version
	@echo BEESIP_PACKAGES=$(BEESIP_PACKAGES) >>$(OWRT_DIR)/tmp/beesip-inc.mk
	@echo EMBEDDED_MODULES=$(EMBEDDED_MODULES) >>$(OWRT_DIR)/tmp/beesip-inc.mk
	@echo OWRT_ADDED_ETCDEFAULT=$(OWRT_ADDED_ETCDEFAULT) >>$(OWRT_DIR)/tmp/beesip-inc.mk
	@#touch $(SOWRTCFG)

force-config-owrt:
	$(RM) -f $(SOWRTCFG)
	@$(BMAKE) config-owrt
	
switch-owrt: download-owrt
	$(call mkdirall)
	@rm -rf $(OWRT_DIR)/files
	@if $(call owrt_env,list) | grep -q "$(TARGET_NAME)"; then \
	  echo n | $(call owrt_env,switch "$(TARGET_NAME)"); \
	else \
	  echo Y | $(call owrt_env,new "$(TARGET_NAME)") ; \
	fi

prepare-owrt: switch-owrt $(SOWRTPREP)
$(SOWRTPREP): $(SPATCHED) $(SFEEDSI) $(SOWRTCFG) $(OWRTTCPREP) $(SKERNPREP)
	touch $(SOWRTPREP)

prepare-toolchain: $(OWRTTCPREP)
$(OWRTTCPREP):
	$(call owrt_testconf)
	@$(BMAKE) -C $(OWRT_DIR) prepare -j$(JOBS)
	touch $(OWRTTCPREP)

config-kernel: prepare-kernel
prepare-kernel: $(SKERNPREP)
$(SKERNPREP): $(PATCHED) $(SOWRTCFG)
	$(call mkdirall)
	$(call owrt_testconf)
	if [ -z "$$($(OWRT_DIR)/scripts/shell.sh env LINUX_DIR)" ]; then echo "Problem with openwrt patch ??" ;exit 1; fi
	@$(BMAKE) -C $(OWRT_DIR) tools/install -j$(JOBS)
	@$(BMAKE) -C $(OWRT_DIR)  target/prereq -j$(JOBS)
	@$(BMAKE) -C $(OWRT_DIR) target/linux/prepare -j$(JOBS)
	@for i in $(EMBEDDED_MODULES); do if ! grep -q "CONFIG_$$i\=y" $$($(OWRT_DIR)/scripts/shell.sh env LINUX_DIR)/.config; then echo "Missing embedded module $$i - possible kernel conf problem"; exit 1; fi; done
	touch $(SKERNPREP)
	
tools:
	$(call owrt_testconf)
	$(MAKE) -C $(OWRT_DIR) tools/install -j$(JOBS)

patch-owrt: $(SPATCHED)
$(SPATCHED): $(SFEEDSU)
	cd $(OWRT_DIR); sh $(PWD)/scripts/apply.sh "$(PWD)" "$(OSTAMP_DIR)" patch-$(OWRT_NAME)
	cd $(OWRT_DIR); sh $(PWD)/scripts/apply.sh "$(PWD)" "$(OSTAMP_DIR)" patch $(OWRT_PATCHES)
	chmod +x $(OWRT_DIR)/scripts/getvar.sh $(OWRT_DIR)/scripts/shell.sh
	touch $(SPATCHED)
	$(RM) -f $(SOWRTCFG)

unpatch-owrt: gitclean

owrt-image:
	if [ -f "$@.gz" ] && ! [ -f "$@" ]; then gunzip <"$@.gz" >"$@"; fi
	if [ -f "$@" ] && ! [ -f "$@.gz" ]; then gzip <"$@" >"$@.gz"; fi
	$(call mkdirall)
	$(call owrt_testconf)
	@for i in $(EMBEDDED_MODULES); do if ! grep -q "CONFIG_$$i\=y" $$($(OWRT_DIR)/scripts/shell.sh env LINUX_DIR)/.config; then echo "Missing embedded module $$i - possible kernel conf problem"; exit 1; fi; done
ifeq ($(VERBOSE),)
	$(BMAKE) -C $(OWRT_DIR) IGNORE_ERRORS=m -j$(JOBS) $(MAKEFORCEI) || $(BMAKE) -C $(OWRT_DIR) V=99 -j1
else
	$(BMAKE) -C $(OWRT_DIR) V=99 -j1
endif
	touch $(SOWRTIMG)

download: download-owrt config-owrt
	$(call mkdirall)
	$(BMAKE) -C $(OWRT_DIR) download -j$(JOBS)
	
ifneq ($(BEESIP_IMG_SQUASHFS),)
squashfs-image: $(BEESIP_IMG_SQUASHFS)
$(BEESIP_IMG_SQUASHFS): $(OWRT_IMG_SQUASHFS)
	$(call mkdirall)
	echo "Copying squashfs image"
	echo $(OWRT_IMG_SQUASHFS)
	$(CP) $(OWRT_IMG_SQUASHFS) $(BEESIP_IMG_SQUASHFS)
	md5sum $(BEESIP_IMG_SQUASHFS) >$(BEESIP_IMG_SQUASHFS).md5sum
endif

ifneq ($(OWRT_IMG_VMDK),)
vmdk-image: $(BEESIP_IMG_VMDK)
$(BEESIP_IMG_VMDK): $(OWRT_IMG_VMDK) kvm-sh
	$(call mkdirall)
	$(call ConvertToVmdk,$(OWRT_IMG_VMDK),$(BEESIP_IMG_VMDK),$(VMDK_TYPE))
	md5sum $(BEESIP_IMG_VMDK) >$(BEESIP_IMG_VMDK).md5sum
else
vmdk-image: $(BEESIP_IMG_VMDK)
$(BEESIP_IMG_VMDK): $(OWRT_IMG_DISK)
	$(call mkdirall)
	$(call ConvertToVmdk,$(OWRT_IMG_DISK),$(BEESIP_IMG_VMDK),$(VMDK_TYPE))
	md5sum $(BEESIP_IMG_VMDK) >$(BEESIP_IMG_VMDK).md5sum
endif
	$(call ConvertToVmdk,$(BEESIP_IMG_DIR)/beesip-data.img,$(BEESIP_DATA_VMDK),$(VMDK_TYPE))
	md5sum $(BEESIP_IMG_DISK) >$(BEESIP_IMG_VMDK).md5sum

vmx-cfg:
	md5sum $(BEESIP_DATA_VMDK) >$(BEESIP_DATA_VMDK).md5sum
	cat $(PWD)/files/beesip.vmx | \
		sed "s/BEESIP_IMG_VMDK/$(shell basename $(BEESIP_IMG_VMDK))/" | \
		sed "s/BEESIP_DATA_VMDK/$(shell basename $(BEESIP_DATA_VMDK))/" \
		>$(BEESIP_VMX)
	md5sum $(BEESIP_VMX) >$(BEESIP_VMX).md5sum
	
ifneq ($(BEESIP_IMG_OVA),)
ova-image: $(BEESIP_IMG_VMDK) $(BEESIP_IMG_OVA)
$(BEESIP_IMG_OVA): $(BEESIP_IMG_VMDK)
	rm -f $(BEESIP_IMG_DIR)/beesip.vmdk $(BEESIP_IMG_DIR)/beesip-ovr.vmdk
	$(call ConvertToVmdk,$(BEESIP_IMG_DISK),$(BEESIP_IMG_DIR)/beesip.vmdk,stream)
	$(call ConvertToVmdk,$(BEESIP_IMG_DIR)/beesip-data.img,$(BEESIP_IMG_DIR)/beesip-ovr.vmdk,stream)
	cd $(BEESIP_IMG_DIR) && size1g="$$(expr 1024 \* 1024 \* 1024)"; \
	beesip_img_size=$$(stat -L -c %s "beesip.vmdk"); beesip_data_size=$$(stat -L -c %s "beesip-ovr.vmdk"); \
	beesip_img_capacity=$$size1g; beesip_data_capacity=$$size1g; \
	sed -e "s#{beesip_disk}#beesip.vmdk#" -e "s#{beesip_data}#beesip-ovr.vmdk#" \
	  -e "s#{beesip_disk_size}#$$beesip_img_size#" -e "s#{beesip_data_size}#$$beesip_data_size#" \
	  -e "s#{beesip_disk_capacity}#$$beesip_data_capacity#" -e "s#{beesip_data_capacity}#$$beesip_data_capacity#" <$(PWD)/resources/template.ovf >$(BEESIP_IMG_OVF)
	cd $(BEESIP_IMG_DIR) && openssl sha1 beesip.vmdk beesip-ovr.vmdk $$(basename $(BEESIP_IMG_OVF)) >$$(basename $(BEESIP_IMG_OVF) .ovf).mf;
	tar -C "$(BEESIP_IMG_DIR)" -h -c "$$(basename $(BEESIP_IMG_OVF))" "$$(basename $(BEESIP_IMG_OVF) .ovf).mf" "beesip.vmdk" "beesip-ovr.vmdk" >$(BEESIP_IMG_OVA)
	cd $(BEESIP_IMG_DIR) && if which ovftool >/dev/null; then rm -f ""tmp_$$(basename $(BEESIP_IMG_OVA))"" && ovftool "$(BEESIP_IMG_OVA)" "tmp_$$(basename $(BEESIP_IMG_OVA))" && rm -f "$(BEESIP_IMG_OVA)" && mv -f "tmp_$$(basename $(BEESIP_IMG_OVA))" "$(BEESIP_IMG_OVA)" ; fi
endif

kvm-sh:
	@echo '#!/bin/sh' >$(BEESIP_KVMSH)
	@echo 'vmdk=$$(dirname $$0)/$(BEESIP_IMG_PREFIX).vmdk' >>$(BEESIP_KVMSH)
	@echo 'data=$$(dirname $$0)/$(shell basename $(BEESIP_DATA_VMDK))' >>$(BEESIP_KVMSH)
	@echo 'if [ -n "$$1" ] ; then vmdk="$$1"; fi' >>$(BEESIP_KVMSH)
	@echo 'if [ -n "$$2" ]; then data="$$2"; fi' >>$(BEESIP_KVMSH)
	@echo 'if [ -n "$$data" ]; then ddisc="-drive file=$$data,index=1,media=disk,if=virtio"; fi' >>$(BEESIP_KVMSH)
	@echo '$(QEMU) -nographic \
		-device ahci,id=ahci0 \
		-drive file=$$vmdk,index=0,media=disk,if=none,id=drive-sata0-0-0 \
		-device ide-drive,bus=ahci0.0,drive=drive-sata0-0-0,id=sata0-0-0 \
		-net nic,model=e1000 -net nic,model=e1000 \
		$$ddsic' \
		>>$(BEESIP_KVMSH)
	
$(IMG_DIR)/$(IB_NAME).tar.bz2:
	$(BMAKE) -C $(OWRT_DIR) target/imagebuilder/install
ib imagebuilder: $(SOWRTIMGB)
ifneq ($(OWRT_IMG_PROFILE),)
	rm -rf $(OWRT_DIR)/tmp/$(IB_NAME)/packages
	ln -s $(OWRT_PKG_DIR) $(OWRT_DIR)/tmp/$(IB_NAME)/packages
	rm -rf $(OWRT_DIR)/tmp/$(IB_NAME)/bin
	ln -s $(OWRT_DIR)/bin $(OWRT_DIR)/tmp/$(IB_NAME)/bin
	rm -f $(OWRT_PKG_DIR)/Packages*
	mkdir -p $(OWRT_DIR)/files
	$(call AddExternalFiles)
	$(call AddFiles)
	@make -C $(OWRT_DIR)/tmp/$(IB_NAME) image PROFILE=$(OWRT_IMG_PROFILE) PACKAGES="$(BEESIP_EPACKAGES)" FILES="$(OWRT_DIR)/files"
	@mkdir -p $(BEESIP_IMG_DIR)
	@$(BMAKE) image-info >$(BEESIP_IMG_DIR)/imageinfo.txt
image-info: $(SOWRTIMGB)
	make -C $(OWRT_DIR)/tmp/$(IB_NAME) package_info PROFILE=$(OWRT_IMG_PROFILE) PACKAGES="$(BEESIP_EPACKAGES)" FILES="$(OWRT_DIR)/files"
endif

ibclean:
	rm -rf $(OWRT_DIR)/tmp/$(IB_NAME) $(IMG_DIR)/$(IB_NAME).tar.bz2

$(SOWRTIMGB): $(IMG_DIR)/$(IB_NAME).tar.bz2
ifneq ($(OWRT_IMG_PROFILE),)
	cd $(OWRT_DIR)/tmp && tar -jxf $(IMG_DIR)/$(IB_NAME).tar.bz2
	touch $(SOWRTIMGB)
endif

fastimg: force-config-owrt beesip-packages ib

clean-ib clean-imagebuilder:
	rm -f $(SOWRTIMGB) $(IMG_DIR)/$(IB_NAME).tar.bz2

ifneq ($(BEESIP_IMG_BIN),)
$(OWRT_IMG_BIN): imagebuilder
bin-image: $(BEESIP_IMG_BIN)
$(BEESIP_IMG_BIN): $(OWRT_IMG_BIN)
	$(call mkdirall)
	if [ -f $(OWRT_IMG_BIN) ] ; \
	then \
		echo "Copying binary image:"; \
		$(CP) $(OWRT_IMG_BIN) $(BEESIP_IMG_BIN); \
		md5sum $(BEESIP_IMG_BIN) >$(BEESIP_IMG_BIN).md5sum; \
	else \
		echo "Binary image not available, not copying it."; \
	fi;
endif

ifneq ($(BEESIP_IMG_FACTORY),)
$(OWRT_IMG_FACTORY): imagebuilder
trx-image: $(BEESIP_IMG_FACTORY)
$(BEESIP_IMG_FACTORY): $(OWRT_IMG_FACTORY)
	$(call mkdirall)
	@echo "Copying factory image"
	@echo $(OWRT_IMG_FACTORY)
	$(CP) $(OWRT_IMG_FACTORY) $(BEESIP_IMG_FACTORY) 
	md5sum $(BEESIP_IMG_FACTORY) >$(BEESIP_IMG_FACTORY).md5sum	
endif

ifneq ($(BEESIP_IMG_DISK),)
disk-image: $(BEESIP_IMG_DISK)
$(BEESIP_IMG_DISK): $(OWRT_IMG_DISK)
	$(call mkdirall)
	if [ -f $(OWRT_IMG_DISK).gz ] && ! [ -f $(OWRT_IMG_DISK) ]; then \
		zcat $(OWRT_IMG_DISK).gz >$(OWRT_IMG_DISK) ; \
	fi
	@echo "Copying disk image"
	@echo $(OWRT_IMG_DISK)
	$(CP) $(OWRT_IMG_DISK) $(BEESIP_IMG_DISK)
	gzip -cf <$(BEESIP_IMG_DISK) >$(BEESIP_IMG_DISK).gz
	md5sum $(BEESIP_IMG_DISK).gz >$(BEESIP_IMG_DISK).gz.md5sum	
	md5sum $(BEESIP_IMG_DISK).gz >$(BEESIP_IMG_DISK).md5sum	
	qemu-img create -f raw $(BEESIP_IMG_DIR)/beesip-data.img 1G
	echo BeeSipData | dd conv=notrunc of=$(BEESIP_IMG_DIR)/beesip-data.img
	gzip -cf <$(BEESIP_IMG_DIR)/beesip-data.img >$(BEESIP_IMG_DIR)/beesip-data.img.gz
	md5sum $(BEESIP_IMG_DIR)/beesip-data.img >$(BEESIP_IMG_DIR)/beesip-data.img.md5sum
	md5sum $(BEESIP_IMG_DIR)/beesip-data.img.gz >$(BEESIP_IMG_DIR)/beesip-data.img.gz.md5sum
endif

ifneq ($(BEESIP_IMG_KERNEL),)
kernel-image: $(BEESIP_IMG_KERNEL)
$(BEESIP_IMG_KERNEL): $(OWRT_IMG_KERNEL)
	$(call mkdirall)
	@echo "Copying kernel image"
	@echo $(OWRT_IMG_KERNEL)
	$(CP) $(OWRT_IMG_KERNEL) $(BEESIP_IMG_KERNEL)
	md5sum $(BEESIP_IMG_KERNEL) >$(BEESIP_IMG_KERNEL).md5sum	
endif

ifneq ($(BEESIP_IMG_SYSUPGRADE),)
sysupgrade-image: $(BEESIP_IMG_SYSUPGRADE)
$(BEESIP_IMG_SYSUPGRADE): $(BEESIP_IMG_KERNEL) $(BEESIP_IMG_SQUASHFS) $(BEESIP_IMG_DISK)
	$(call mkdirall)
	if [ -f $(OWRT_IMG_SYSUPGRADE) ] ; \
	then \
		echo "Copying sysupgrade image:"; \
		$(CP) $(OWRT_IMG_SYSUPGRADE) $(BEESIP_IMG_SYSUPGRADE); \
		md5sum $(BEESIP_IMG_SYSUPGRADE) >$(BEESIP_IMG_SYSUPGRADE).md5sum; \
	fi;
# ifneq ($(SYSUPGRADE_GZ),)
#	gzip -f $(BEESIP_IMG_SYSUPGRADE)
#	md5sum $(BEESIP_IMG_SYSUPGRADE).gz >$(BEESIP_IMG_SYSUPGRADE).gz.md5sum
# else
#	md5sum $(BEESIP_IMG_SYSUPGRADE) >$(BEESIP_IMG_SYSUPGRADE).md5sum
# endif
endif

ifneq ($(BEESIP_IMG_ISO),)
iso-image: $(BEESIP_IMG_ISO)
$(BEESIP_IMG_ISO): $(OWRT_IMG_ISO)
	$(call mkdirall)
	@echo "Copying ISO image"
	@echo $(OWRT_IMG_ISO)
	$(CP) $(OWRT_IMG_ISO) $(BEESIP_IMG_ISO)
	md5sum $(BEESIP_IMG_ISO) >$(BEESIP_IMG_ISO).md5sum
endif

beesip-packages: 
	rm -f $(OWRT_DIR)/tmp/.package*
	@$(BMAKE) -k -C $(OWRT_DIR) -j$(JOBS) $(foreach pkg,beesip uciprov,package/$(pkg)/clean) -j3
	@$(BMAKE) -k -C $(OWRT_DIR) -j$(JOBS) $(foreach pkg,beesip uciprov,package/$(pkg)/install) -j3
	@$(BMAKE) -k -C $(OWRT_DIR) package/index

beesip-image: beesip-images
beesip-images: $(BEESIP_IMAGES)
	@echo "Done! Images at *$(BEESIP_IMAGES)*."
	
all-packages: config-owrt
	$(call owrt_testconf)
	$(MAKE) config-owrt OWRT_CONFIG_SET='$(OWRT_CONFIG_SET) ALL=y' OWRT_CONFIG_UNSET='$(OWRT_CONFIG_UNSET) PACKAGE_numpy'
	@$(BMAKE) -C $(OWRT_DIR) package/compile package/install package/index -j$(JOBS) IGNORE_ERRORS=m -i

packages: config-owrt
	$(call owrt_testconf)
	@$(BMAKE) -C $(OWRT_DIR) package/compile package/install package/index -j$(JOBS) IGNORE_ERRORS=m

beesip: $(SOWRTPREP)

kernel_menuconfig:
	@$(BMAKE) -C $(OWRT_DIR) kernel_menuconfig 

kernel:
	@$(BMAKE) -C $(OWRT_DIR) target/linux/compile -j$(JOBS)

clean-kernel:
	@$(BMAKE) -C $(OWRT_DIR) target/linux/clean -j$(JOBS)

prov-tgz: $(KVM_TFTP_DIR)/tgz.fd
$(KVM_TFTP_DIR)/tgz.fd:
ifneq ($(KVM_TFTP_DIR),)
	@mkdir -p $(KVM_TFTP_TMP)/etc
	@echo remove_uris >$(KVM_TFTP_DIR)/opkg.fd
	@for f in $(OWRT_DEFAULT_FEEDS) $(OWRT_ADDED_FEEDS); do echo $$f; done | \
	  while read proto name rest; do [ -n "$$name" ] && [ -d "$(OWRT_PKG_DIR)/$$name" ] && echo add_uri src $${name}_local file:///mnt/pkg/$$name; done >>$(KVM_TFTP_DIR)/opkg.fd; true
	@mkdir -p $(KVM_TFTP_TMP)/etc/config
	@cp files/fstab.dev $(KVM_TFTP_TMP)/etc/config/fstab
	@$(call AddUciprovOpkg,$(UCIPROV_DEVOPKG)) >>$(KVM_TFTP_DIR)/opkg.fd
	@cp $(KVM_TFTP_DIR)/opkg.fd $(KVM_TFTP_DIR)/opkg.nofd
	@$(call AddUciprovDefaults,$(UCIPROV_DEVDEFAULTS)) >$(KVM_TFTP_DIR)/uci.fd
	@$(call AddUciprovServices,$(UCIPROV_DEVSERVICES)) >$(KVM_TFTP_DIR)/services.fd
	@mkdir -p $(KVM_TFTP_DIR) && cd $(KVM_TFTP_TMP) && tar -czf $(KVM_TFTP_DIR)/tgz.fd .
	@echo Prov data in $(KVM_TFTP_DIR)
endif

test-kvm: prov-tgz
	@mkdir -p $(KVM_DIR)
	@if [ ! -f $(KVM_DIR)/beesip-data.vmdk ]; then $(call ConvertToVmdk,$(BEESIP_DATA_VMDK),$(KVM_DIR)/beesip-data.vmdk,$(VMDK_TYPE)) fi
	@mkdir -p $(KVM_DIR)/overlay $(KVM_DIR)/tmp $(KVM_DIR)/data
	$(KVM)	-nographic \
		-virtfs local,path=$(PWD),$(VIRTFS_SECURITY),mount_tag=src \
		-virtfs local,path=$(BEESIP_IMG_DIR),$(VIRTFS_SECURITY),mount_tag=img \
		-virtfs local,path=$(OWRT_PKG_DIR),$(VIRTFS_SECURITY),mount_tag=pkg \
		-virtfs local,path=$(KVM_DIR)/tmp,$(VIRTFS_SECURITY),mount_tag=tmp \
		-device ahci,id=ahci0 \
		-drive file=$(BEESIP_IMG_VMDK),index=0,media=disk,if=none,id=drive-sata0-0-0 \
		-device ide-drive,bus=ahci0.0,drive=drive-sata0-0-0,id=sata0-0-0 \
		-drive file=$(KVM_DIR)/beesip-data.vmdk,index=1,media=disk,if=virtio

clean-kvm:
	@if [ -d "$(KVM_DIR)" ]; then rm -rf "$(KVM_DIR)"; fi
	
expect-kvm:
	expect -f resources/testkvm.expect >$(OWRT_DIR)/qemu.log 

testdeps:
	@if [ -n "$(SUBPKG)" ]; then printf "\nDependencies for $(SUBPKG): "; else printf "\nDependencies for $(PKG):"; fi
	@for p in $(shell echo $(BEESIP_DEPENDS) | tr -d '+@'); do \
		if [ -n "$$p" ]; then \
			if ! $(BMAKE) -s testdeps PKG=$$p SUBPKG=$(SUBPKG)/$(PKG); then \
				exit 2; \
			fi; \
		fi; \
	done 2>/dev/null
	@if grep -q $(shell echo $(PKG) | tr -d '+@') $(OWRT_DIR)/.config; then \
		printf "$(PKG) OK, "; \
	else \
		printf "\n$(PKG) missing!\n" ;\
		exit 2; \
	fi
	@if [ -z "$(SUBPKG)" ]; then echo; fi
