# This is example UCI config for provisioning
# This file is DEFAULT for all boxes
# Edit and uncomment lines which you need to apply to all boxes
# You can use {macro} substitution (see uciprov without parameters)
# Empty lines and hashed lines are comments
# Use this file only for defaults for all boxes with same codename!

# Set Logging server to some static.ip
#set system.@system[0].log_ip=192.168.1.1

# Set right timezone
#set system.@system[0].timezone=Europe/Prague

# Set NTP parameters
#set system.ntp=timeserver
#set system.ntp.server=192.168.1.1

# Set right domains for DHCP and enable DHCP server on LAN (wifi)
#set dhcp.@dnsmasq[0].domain=some.domain
#set dhcp.lan.ignore=0

# Set provisioning uri to static list of uris (based on default, board and fqdn)
#set uciprov.global.uri_retrieval_protocols=static
#set uciprov.global.reboot_delay=5
# First we will try default uri for all boxes
#set uciprov.global.uri_static1='http://{hostname}:{boxid}@provserver.some.doma.in/default/{m}.{fde}'
# Second board specific
#set uciprov.global.uri_static2='http://{hostname}:{boxid}@provserver.some.doma.in/{board}/{m}.{fde}'
# Last fqdn of this box
#set uciprov.global.uri_static3='http://{hostname}:{boxid}@provserver.some.doma.in/{fqdn}/{m}.{fde}'

# Another example of static uris, based on default, codename and MAC
# First we will try default uri for all boxes
#set uciprov.global.uri_static1='http://{hostname}:{boxid}@provserver.some.doma.in/default/{cn}/{m}.{fde}'
# Second board specific
#set uciprov.global.uri_static2='http://{hostname}:{boxid}@provserver.some.doma.in/{board}/{cn}/{m}.{fde}'
# Last fqdn of this box
#set uciprov.global.uri_static3='http://{hostname}:{boxid}@provserver.some.doma.in/{mac}/{m}.{fde}'

# Sysupgrade can be forced even on non-fd box. In default situation, sysupgrade will be applied only on factory default hosts
#set uciprov.sysupgrade.only_on_fd=0

# You can speedup deploying by forcing reboot imediately after deploy. Default is to wait for some time to be able to log in
#set uciprov.global.reboot_after_cfg_apply=0

# You can force opkg module to update/install some packages on each boot, not only fd
#set uciprov.opkg.only_on_fd=0

# Turn firewall off. Be carefull!
#set firewall.@zone[1].input=ACCEPT
#set firewall.@zone[1].output=ACCEPT
#set firewall.@zone[1].forward=ACCEPT

# Configure zabbix agent to reach zabbix server and to be able to be discovered
#set zabbix_agentd.config=zabbix_agentd
#set zabbix_agentd.config.enable=true
#set zabbix_agentd.config.server=zabbix.doma.in
#set zabbix_agentd.config.serveractive=zabbix.doma.in
#set zabbix_agentd.config.listenport=10050
#set zabbix_agentd.config.hostnameitem=system.hostname
#set zabbix_agentd.config.allowroot=1
#set zabbix_agentd.config.enableremotecommands=1
#set zabbix_agentd.config.logremotecommands=1
#set zabbix_agentd.config.timeout=30

# You can add some zabbix user parameters. Be carefull and do not add same parameter two times!
#add zabbix_agentd userparm
#set zabbix_agentd.@userparm[0]=userparm
#set zabbix_agentd.@userparm[0].key='my.test.param'
#set zabbix_agentd.@userparm[0].internal=0
#set zabbix_agentd.@userparm[0].cmd='dmesg | grep "$1" | tail -1'


