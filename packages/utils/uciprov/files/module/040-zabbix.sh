#!/bin/sh

. /usr/share/libubox/jshn.sh

if uciprov_initmodule1 zabbix; then
	#uciprov_hook_add uciprov_geturi get_uri_zabbix
	echo -n
fi

if uciprov_initmodule2 zabbix; then
	#uciprov_hook_add uciprov_fetchuri fetch_uri_zabbix
	echo -n
fi

uciprov_zabbix(){
    # Nothing here yet
    echo 
}

zabbix_help(){
  echo "Zabbix module for uciprov"
}

zabbix_get_host(){
	local user pw url
	config_load uciprov
	config_get user zabbix apiuser
	config_get pw zabbix apipw
	config_get url zabbix url
	[ -z "$zauth" ] && zauth=$(zabbix_api_login "$url" "$user" "$pw")
	[ -z "$auth" ] && return false
	json_init
	json_add_string "jsonrpc" "2.0"
	json_add_string "method" "host.get"
	json_add_string "auth" "$zauth"
	json_add_object "params"
	json_add_boolean selectInventory 1
	json_add_boolean selectMacros 1
	json_add_string "output" "extend"
	json_add_object "filter"
	json_add_array "host"
	json_add_string "" "$1"
	json_close_array
	json_close_object
	json_close_object
	json_add_int "id" 1
	post=$(json_dump)
	result=$(curl --capath /etc/ssl/certs -f -s -u "$user:$pw" -X POST -H 'Content-Type:application/json' -d"$post" "$url") && echo $result
}

zabbix_api_login(){
	local post result auth
	json_init
	json_add_string "jsonrpc" "2.0"
	json_add_string "method" "user.login"
	json_add_object "params"
	json_add_string "user" "$2"
	json_add_string "password" "$3"
	json_close_object
	json_add_int "id" 1
	post=$(json_dump)
	result=$(curl --capath /etc/ssl/certs -f -s -u "$2:$3" -X POST -H 'Content-Type:application/json' -d"$post" "$1") && \
	   json_load "$result" && json_get_var auth "result" && echo $auth
}

get_uri_zabbix2(){
	local hostname keys i var
	hostname=$(getmacro fqdn)
	json_load "$(zabbix_get_host $hostname)"
	json_get_keys keys result
	json_select result
	json_select $keys
	json_get_keys keys inventory
	json_select inventory
	for i in $keys; do
		json_get_var var "$i" && [ -n "$var" ] && setmacro "$i" "$var"
	done
}

fetch_uri_zabbix2(){
	local proto hostname keys i var
	hostname=donalisa
	json_load "$(zabbix_get_host $hostname)"
	json_get_keys keys result
	json_select result
	json_select $keys
	json_get_keys keys inventory
	json_select inventory
	for i in $keys; do
		json_get_var var "$i" && [ -n "$var" ] && setmacro "$i" "$var"
	done
}

