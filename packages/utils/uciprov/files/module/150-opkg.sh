#!/bin/sh

uciprov_initmodule2 opkg opkg

uciprov_opkg(){
	local uri=$(uciprov_runmodule2 "$@") || return 1;
	local replace src update install
	
	config_load uciprov
	fetch_uri "$uri" |  eval far $(getmacros) >$UCITMP/opkg || return 1;
	
	grep -v '^#' $UCITMP/opkg | while read cmd arg1 arg2 arg3 arg4 restargs; do
	case $cmd in
remove_uris)
	rm -f /etc/opkg/uciprov-*.conf
	;;
remove_uri)
	rm -f /etc/opkg/uciprov-${arg2}.conf
	;;
add_uri)
	if [ -z "$arg1" ] || [ -z "$arg2" ] || [ -z "$arg3" ]; then
	  mdbg 2 opkg "Bad uri to add ($arg1 $arg2 $arg3)"
	else
	  echo "$arg1 $arg2 $arg3" >>/etc/opkg/uciprov-${arg2}.conf
	fi
	;;
update)
	opkg update
	;;
option)
	grep -v "^option.*$arg1" /etc/opkg.conf >$UCITMP/opkg.conf && cp $UCITMP/opkg.conf /etc/opkg.conf
	echo "option $arg1 $arg2" >>/etc/opkg.conf
	;;
install)
	opkg install $arg1 $arg2 $arg3 $arg4 $restargs
	;;
installtmp)
	opkg install -d ram $arg1 $arg2 $arg3 $arg4 $restargs
	;;
remove)
	opkg remove $arg1 $arg2 $arg3 $arg4 $restargs
	;;
	esac
	done
}

opkg_help(){
	echo "Uciprov OPKG autoinstall module"
}

