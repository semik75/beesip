# UCI provisioning for OpenWrt

UCI provisioning system is tool for OpenWrt distribution, which will allow OpenWrt box to be automaticaly configured via network.
There are several ways and methods how to discover what to provision. Uciprov is modular, shell based provisioning which can be easily expanded.
Most common usage is to download UCI configuration to the box just after factory defaults. But there are much more ways how to use Uciprov.

## How it works ##

Uciprov has three ways how to be run. First is simple from init script, second is based on interface state change and third is regular interval check.
Uciprov default options can be overwritten during provision, so second stage provision will be based on new parameters. This is common way how to provision 
more kind of devices based on IP range, DHCP or box type.

### Basic run procedure ###

When uciprov is started (from init or from DHCP client process), it will collect all informations from uciprov config, tries to resolve all macros and get all possible uris.
When uri is available, it is fetched. Fetched file is expected to be text input, which is in uci batch format. File can contain macros which will be resolved during provision. See above.
For example:
```
set network.wan.hostname={hostname}
set network.lan.dns={dns}
set network.lan.dns_search={domain}
set network.wan.vendorid=Eduroam
set network.@switch[0]=switch
set network.@switch[0].name=switch0
set network.@switch[0].reset=1
set network.@switch[0].enable_vlan=0
set network.@switch_vlan[0]=switch_vlan
set network.@switch_vlan[0].device=switch0
set network.@switch_vlan[0].vlan=1
set network.@switch_vlan[0].vid=1
set network.@switch_vlan[0].ports="0 1 2 3 4"
```

### Stages ###

Stage1 is used for fetching informations about available uris and macros. Stage2 is used to resolve uris and get informations from there.

## Uciprov macros ##

### Builtin macros ###

Uciprov can use macros for stage 2. Some macros are directly in uciprov, but modules can set any macros.
Default macros:
```
{hostname}		# Hostname
{domain}		# Domain from DHCP
{dns}			# DNS server IP (first of list)
{fqdn}			# Fqdn of host
{mac}			# Mac address of provisioning interface (aa-bb-cc-dd-ee-ff)
{mac2}			# Mac address of provisioning interface (aabbccddeeff)
{mac3}			# Mac address of provisioning interface (aa:bb:cc:dd:ee:ff)
{ip}			# IP address of provisioning interface
{fd}			# Box in factory defaults (0/1)
{fde}			# Expands to 'fd' or 'nofd' if box is or is not in factorydefaults
{ifname}		# Provisioning interface name (like eth0)
{oifname}		# Owrt Provisioning interface name (like wan)
{sr}			# Revision of fistribution (like git rev)
{cn}			# Codename of distribution (like barrier_breaker)
{rl}			# Release of distribution (like 14.07)
{tgt}			# Distribution target and subtarget (like x86_64/generic)
{cpu}			# CPU (like x86_64)
{sub}			# Subtarget
{board}			# For some boxes, this is board identification string. Like tplink-wdr3600-v1. This can be used to fetch right image for box
{ovr}			# Overlay mounted (0/1)
{nettime}		# Network time obtained (0/1)
{uci_uris}		# All uciprov uris
{uri_static1}		# static uri 1
{uri_static2}		# static uri 2
{uri_static3}		# static uri 3
{dhcp_uri}		# Uri fetched from DHCP server
{m}			# Module which performs given action (eg. uci for uciprov, tgz for tgz module)
{boxid}			# Persistent, randomly generated hash for this box. This id is persistent up to next factory defaults.

```

### User macros ###

There can be more macros defined in config. Macro can be either static or dynamic. Static (default) means that when value is empty, user function will be called and value
of macro will be saved from function stdout. In dynamic macros,  value will be obtained from function every call. It is possible to redefine even builtin macros from this config.

```
# Configuration of user macros
# First time, 'uname -m' will be called and its output will be saved as value.
config macro machine
	option func				'uname -m'

# Value will never be saved. Every run will be evaulated
config macro tun0ip
	option func				'ifconfig tun0| grep -o "inet addr\:[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" | grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*"'
	option type				'dynamic'
```

## Uciprov parameters ##

There are lot of parameters which affect how Uciprov works. It is important to know that basic parameters are EMBEDDED into image so changing them afterward has no effect
for factory-defaults boxes. It is important to setup good configuration to fit your needs. Uciprov defaults can be changed using standard openwrt config procedure.

### Global UCI parameters ###
```

	# Enable uciprov at all. If set to zero, uciprov will do nothing.
        option enable                           '1'

	# When box is in factory defaults (first run of uciprov after factory reset), This option is 1
	# After first run, this is automaticaly set to 0
	option factorydefault			'1'

	# Set of protocols to fetch informations from separated by spaces
	# Order of protocols cannot be changed here. There are hardcoded into scripts.
	# It is possible to turn on or off
        option uri_retrieval_protocols          "dnssec dns dhcp static"

	# Urls to fetch data from. There are more urls, which can be global or device specific.
	# They are evaulated in order 1,2,3. All defined url will be tested.
	# You can use macros inside urls. Use 'uciprov' command without parameters to see all macros.
	# {hostname} 'somehost'				Hostname from DHCP or static config
	# {domain}    'some.doma.in'			Domain
	# {mac}       'so-me-ma-ca-dd-rr'		Mac addr
	# {ip}	 'x.x.x.x'				IP of wan
	# {fd} '1'					This will be 1 or 0 according to 'factorydefault' option

	# This url will be expanded to http://beesip-porv.domain_from_dhcp/kvm-686/fd/uci in first-time run and http://beesip-porv.domain_from_dhcp/kvm-686/nofd/uci in other cases
	# Because uri does not contain device specific macros, it will be applied to all boxes!
	# For uci provisioning, module name is uci
	option uri_static1 			http://beesip-prov.{d}/{tgt}/{fde}/{m}
	
	# Example to feed more macros into php script
	# option uri_static1			"http://server/script.php?mac={mac}&board={board}&fd={fd}&module={m}"

	# This settings will be applied to group of boxes with same CPU. Like uci-x86 for UCI , tgz-x86 for tgz module
	option uri_static2 			http://beesip/{m}-{cpu}

	# This is box specific settings, regardless of factorydefaults. Will be fetched and applied any time.
	option uri_static3 			http://beesip/{mac}/uci

	# prefix which is searched when using DNS provisioning
	option dns_prefix			"beesip"

	# Interface which is used for provisioning. In most cases, wan
	option interface			"wan"

	# If set to 1, box is rebooted after provisioning. Be carefull for endless booting!
        option reboot_after_cfg_apply           0
	
	# In some cases, it can take a while to initialize /overlay. If this is set to number, uciprov will wait maximum this number of seconds until /overlay is mounted in background
	option wait_for_overlay			120

	# Before rebooting, it is possible to wait this number of seconds (to be able to recover some endless loop)
	option reboot_delay			60

	# If set 0 number bigger than 0, uciprov will repeat periodicaly to fetch config. 
	# This can be used to periodicaly fetch new config (in this example, each 120 seconds)
	option repeat				120

	# If set to 1, uciprov will continue in periodic uri fetching. 
	# When set to 0, uciprov will stop to fetch next uri after first successful provisioning data
	option repeat_on_success		0

```

### Protocol hooks for getting uris ###

Uciprov is modular and there can be more modules to fetch uris from. For example, it is possible to set "bootfile" parameter of DHCP to override UCI uris. There are three modules by default: STATIC, DHCP and DNS.
Static is configured by uciprov config (uri_static1, uri_static2, uri_static3). DHCP is configured from bootfile parameter of DHCP.
Next to this, it is possible to create TXT record. Uciprov will try to get TXT record "dns_prefix" and fetch uri from there. All uris are glued and after it, fetched.

### Modules functions ###

Next to this, uciprov is able to do some more deployment than only UCI. Modules can hook to stage1 or stage2 hooks.

#### DHCP module ####

DHCP module is responsible for getting informations from DHCP server. This is not DHCP client but it uses DHCP client to ask for DHCP options. 

#### DNS module ####

DNS module is responsible for getting informations from DNS server. It can use either DNS or DNSSEC protocol. 

#### STATIC module ####

STATIC module is responsible for getting informations staticaly from uci config.

#### Common config for all modules ####
Every module will read basic configuration options from its container. Next to this, there can be module specific parameters.
```
config uciprov somemodule
      # If module should be enabled at all.
      option enable   		1		
      # If it should apply any time or only in factory defaults
      option only_on_fd 	1
      # Uri for module. If this parameter is missing, static or dhcp uris are used and module name is expanded by {m} macro.
      option uri "http://somewhere/{cpu}/{board}/config"
      # Version is used for any module to know if module was already applied. After successful module run, this option is changed to md5 sum of received data from its uri.
      # If md5 of fetched file is same as this config option, module is skipped
      option version 1.2.3
```

#### Sysupgrade module ####

Sysupgrade module is used to automaticaly upgrade boxes from common urls. It takes config from sysupgrade part of uciprov config:
```
config uciprov sysupgrade
	option enable				1
	option only_on_fd			1
	option uri				"http://beesip/images/{board}/su.img"
	option version				0.2.4
```
This config instructs sysupgrade module to upgrade system only when box is in factory defaults state. When there is no limit on factory defaults, file uri will be fetched any time!
After it, md5 is computed and compared with 'version' option. If it differs, it is sysupgraded.

#### Tgz module ####

Tgz module is created to deploy tgz files for configurations which do not support UCI. Works similar to sysupgrade. If there is uri available to fetch, it is fetched and untared in root.
```
config uciprov tgz
	option enable				1
	option only_on_fd			1
	option uri				"http://beesip/files/files.tgz"
	# When to run module.			{preinit|postinit|preapply|peruri}
	# preinit. Module will be run one time just after stage1 when all uris are known
	# postinit. Module will be run one time just after uci apply
	# preapply. Module will be run one time just before uci apply
	# peruri. Module will be run for each uri, discovered by stage1. Eg. three times for static1,static2,static3
	option run_on				"preinit"
	option version				0.2.4
	# If set to 1, there will be macro expansion enabled for fetched data.
	option macroexpand			1
```
This files will be deployed to all boxes after factory defaults only.

#### Recovery module ####
This module works for box recovery if everything else fails. It will fetch given uri and read data from it. If data are same as flag, defined in config, emergency script is run.
Emergency script could be for example stop firewall, restart telnet server or any other script. If DHCP is set to 1, UCI uris are fetched from DHCP "bootfile" too.
```
config uciprov recovery
	option enable 				1
	option uri				"http://beesip/fd/fd.flag"
	option fd_flag				"1234"
	option dhcp				1
	option script				""
```

#### SslDeploy module ####
This module deploys SSL key and certificate into box. Key will be generated directly in box. CSR request will be uploaded and certitficate will be downloaded after signing from remote server.

```
config uciprov ssldeploy
	option enable 				1
	option algo				rsa
	option bits				2048
	option subject				CN={mac2}
	option keyfile				/etc/ssl/box.key
	option crtfile				/etc/ssl/box.crtfile
	option csrfile				/etc/ssl/box.csr
	option keyowner				root:root
	option keychmod				700
	option push_uri				https://csrreq.host/post/{mac2}
	option fetch_uri			https://crtget.host/{mac2}
```

#### Services module ####
This module enable to disable/enable services during provision. It will fetch service control file (see above) and do what is needed there.
```
config uciprov services
	option enable 				1
	option uri				"http://beesip/services"
```
Service control file:
```
#service action [condaction]
#action: start|stop|enable|disable
#condaction: fileexists filename
#will run this action only if filename exists
#examples
kamailio stop
kamailio disable
radsecproxy start fileexists /etc/radsecproxy.key
```
#### Opkg module ####
This module can automaticaly setup opkg repositories, update package info and install some packages automaticaly, during uciprov. If replace_repo is 1,
system repositories will be cleared. In other way, package lists are added to list. There can be any number of packages to install/upgrade automaticaly.
```
config uciprov opkg
	option enable 		1
	option uri				"http://beesip/opkg"
```
Opkg control file:
```
remove_uri 'bb_base'
add_uri src/gz bb_base http://downloads.openwrt.org/barrier_breaker/14.07/ar71xx/generic/packages/packages/
option http_proxy http://proxy:3128/
update
install pkg1 pkg2 pkg3 ....
remove pkg1 pkg2 pkg2 ...
```

#### Mgmt module ####
This module can automaticaly setup SSH authorized keys and other management protocols in future. It checks its uri and expect file which will be copied to
/etc/dropbear/authorized_keys. Next to this, root password will be set to random password and saved to file if save_pw is set. If disable_ssh_pwauth is set to 1,
module will disable password auth for dropbear.
```
config uciprov mgmt
	option enable 		1
	option set_root_pw	1
	option authorized_keys /etc/dropbear/authorized_keys
	option save_pw /etc/rootpw
	option disable_ssh_pwauth	1
```

#### Zabbix module ####
This module is for getting information about host from zabbix server. It can obtain uris and macros for stage1 and can get information for stage2. This module can be connected
to zabbix_agentd package. Together with autoregistration/discovery, this is very strong tool.
After boot, Zabbix uci configuration is obtained. Next to this, zabbix_agentd connects to zabbix server and tries to autoregister. Zabbix can be configured to run some actions
after successful registration. If administrator wants precise control over process, host can be created even manualy in Zabbix and disable autoregistration. After registration,
zabbix module is able to fetch informations about host from zabbix server and even send some data using Zabbix API.

```
config uciprov zabbix
	option enable 				0
	option uri				"http://zabbix/api_jsonrpc.php"
	option apiuser				"uciprov"
	option apipw				"uciprovpw"
```

### Writing hooks ###

Uciprov is prepared to use hooks. Each hook is called at specified event. Module can hook to this event and do some work.
It is enough to catch hook in module:

```
get_uri_dhcp(){
	local duri
	duri=$(get_dhcp_var $interface bootfile)
	setmacro uci_uris "$duri"
	setmacro dhcp_uri "$duri"
}

boot_hook_add uciprov_geturi get_uri_dhcp

```

Each time uciprov will try to get uciprov uri, it will call function get_uri_dhcp which will set macros uci_uris and dhcp_uri. These macros will be available later for expansion in uris.

### Available hooks ###

#### uciprov_geturi ####

This hook is called at each stage1 of uciprov when needed to fetch uris for stage2.

#### uciprov_fetchuri ####

This hook is called for each fetching of uri. Uris format is protocol://uri . Module can be hooked to some protocol. 

#### uciprov_testuri ####

This hook is called before each fetching of uri to test if it is reachable. If it is not defined, fetchuri is used.

#### uciprov_peruri ####

This hook is called per each uri discovered by stage1. Actual uri is parameter of function

#### uciprov_preinit ####

Called before rest of init script. Just after setting macros and uris from stage1

#### uciprov_preapply ####

Called before uci apply one time.

#### uciprov_postapply ####

When apply of at least one uri was successfull, this hook will be called.

## Deploying strategies ##

Uciprov can be configured very differently according your needs. Basic scenarios are here.

### Unsecure deployment ###

Unsecure deployment does not take care about certificates or authentication. Any box can be provisioned and there is no manual control which box will be provisioned.
Any device will get infromations from provisioning server. It is highly recomended to make limitation on provisioning server based on source IP of boxes because it can contain secure informations.
Take care about provisioning during non-factory defaults. Any device can be controlled easily by attacker who will serve right urls or get data directly via browser.
Most sensitive provisioning data should be in device specific container because it is not so easy to guess mac address.

```
        option enable                           '1'
        option uri_retrieval_protocols          "dns dhcp static"
	option uri_static1 			http://beesip-prov.{d}/{tgt}/{fde}/{m}
	option uri_static2 			http://beesip-prov.{d}/{tgt}/{mac}/{fde}/{m}
	option uri_static3 			http://beesip-prov.{d}/{tgt}/{ip}/{fde}/{m}

```

### Half-secure deployment ###

In this scenario, we will use HTTPS. So provisioning uris will not be shown for attacker and it is harder to gess them. But we still trust all new devices.

```
        option enable                           '1'
        option uri_retrieval_protocols          "dns dhcp static"
	option uri_static1 			https://beesip-prov.{d}/someunusualstring/{tgt}/{fde}/{m}
	option uri_static2 			https://beesip-prov.{d}/someunusualstring/{tgt}/{mac}/{fde}/{m}
	option uri_static3 			https://beesip-prov.{d}/someunusualstring/{tgt}/{ip}/{fde}/{m}

```

### Secure deployment ###

We do not trust new devices. Administrator has to approve new device by adding its credentials into HTTPS server configuration. New devices will be unconfigured until approved.
Each device will use its own randomly generated boxid. We have to check username and password in HTTPS server.

```
        option enable                           '1'
        option uri_retrieval_protocols          "dnssec static"
	option uri_static1 			https://{boxid}:{mac}@beesip-prov.{d}/{tgt}/{fde}/{m}
	option uri_static2 			https://{boxid}:{mac}@beesip-prov.{d}/{tgt}/{mac}/{fde}/{m}
	option uri_static3 			https://{boxid}:{mac}@beesip-prov.{d}/{tgt}/{ip}/{fde}/{m}

```

