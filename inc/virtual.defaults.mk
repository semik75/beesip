OWRT_CONFIG_SET += VDI_IMAGES=y VMDK_IMAGES=y \
		TARGET_OVERLAY_EXT4_EXTERNAL=y TARGET_OVERLAY_EXT4_DEVICE=\"/dev/vda\,/dev/sdb" \
		TARGET_ROOTFS_PARTSIZE=300 TARGET_EXT4_MAXINODE=60000

EMBEDDED_MODULES += VIRTIO_BLK SCSI_VIRTIO VIRTIO_NET VIRTIO_CONSOLE VIRTIO \
		VIRTIO_PCI VIRTIO_BALLOON VIRTIO_MMIO \
		NET_9P NET_9P_VIRTIO 9P_FS 9P_FS_POSIX_ACL \
		IDE IDE_GD IDE_GD_ATA

# VMDK type.
#VMDK_TYPE = stream

# Generate OVA
IMG_OVA = 1

SYSUPGRADE_GZ = 1

BEESIP_PACKAGES += \
	less=y \
	kmod-vmxnet3=y kmod-e1000e=y \
	rtpproxy=y keepalived=y strace=y

OWRT_ADDED_FILES += virtual-files/network_lan_wan.uci:/etc/config/network virtual-files/fstab.virtual:/etc/config/fstab
