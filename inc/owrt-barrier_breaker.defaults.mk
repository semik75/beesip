
OWRT_GIT="git://git.openwrt.org/14.07/openwrt.git"

USE_CUSTOM_FEEDS=1

$(eval $(call AddDefaultOpenWrtFeed,src-git packages https://github.com/openwrt/packages.git;for-14.07))
$(eval $(call AddDefaultOpenWrtFeed,src-git luci http://git.openwrt.org/project/luci.git;luci-0.12))
$(eval $(call AddDefaultOpenWrtFeed,src-git routing https://github.com/openwrt-routing/packages.git;for-14.07))
$(eval $(call AddDefaultOpenWrtFeed,src-git telephony https://github.com/openwrt/telephony.git;for-14.07))
$(eval $(call AddDefaultOpenWrtFeed,src-git oldpackages http://git.openwrt.org/14.07/packages.git))
$(eval $(call AddDefaultOpenWrtFeed,src-git management https://github.com/openwrt-management/packages.git;for-14.07))

OWRT_CONFIG_UNSET += PACKAGE_asterisk18


