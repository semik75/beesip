BEESIP_NODEFAULTS=generic

ifeq ($(OWRT_NAME),trunk)
	ASTPKG=asterisk13
else
	ASTPKG=asterisk11
endif

KAMPKG=kamailio4

ifeq ($(OWRT_NAME),barrier_breaker)
	BEESIP_PACKAGES+=\
		$(ASTPKG)-gui=y \
		$(ASTPKG)-app-chanisavail=y $(ASTPKG)-app-chanspy=y $(ASTPKG)-app-exec=y $(ASTPKG)-app-minivm=y \
		$(ASTPKG)-app-originate=y $(ASTPKG)-app-read=y $(ASTPKG)-app-readexten=y $(ASTPKG)-app-sayunixtime=y \
		$(ASTPKG)-app-stack=y $(ASTPKG)-app-system=y \
		$(ASTPKG)-app-talkdetect=y $(ASTPKG)-app-verbose=y $(ASTPKG)-app-waituntil=y $(ASTPKG)-app-while=y \
		$(ASTPKG)-chan-iax2=y $(ASTPKG)-chan-dongle=y $(ASTPKG)-codec-a-mu=y \
		$(ASTPKG)-codec-alaw=y $(ASTPKG)-codec-g722=y $(ASTPKG)-codec-g726=y $(ASTPKG)-codec-gsm=y \
		$(ASTPKG)-curl=y \
		$(ASTPKG)-format-g726=y $(ASTPKG)-format-gsm=y \
		$(ASTPKG)-format-g729=y $(ASTPKG)-format-sln=y $(ASTPKG)-func-blacklist=y \
		$(ASTPKG)-func-channel=y $(ASTPKG)-func-cut=y $(ASTPKG)-func-db=y $(ASTPKG)-func-devstate=y $(ASTPKG)-func-extstate=y \
		$(ASTPKG)-func-global=y $(ASTPKG)-func-shell=y $(ASTPKG)-func-uri=y $(ASTPKG)-func-vmcount=y \
		$(ASTPKG)-pbx-ael=y $(ASTPKG)-pbx-spool=y $(ASTPKG)-res-ael-share=y $(ASTPKG)-res-agi=y \
		$(ASTPKG)-res-musiconhold=y $(ASTPKG)-cdr=y $(ASTPKG)-sounds=y $(ASTPKG)-voicemail=y $(ASTPKG)-cdr-sqlite3=y $(ASTPKG)-cdr-csv=y \
		$(ASTPKG)-chan-ooh323=y
else ifeq ($(OWRT_NAME),chaos_calmer)
	BEESIP_PACKAGES+=\
		$(ASTPKG)-gui=y \
		$(ASTPKG)-app-chanisavail=y $(ASTPKG)-app-chanspy=y $(ASTPKG)-app-exec=y $(ASTPKG)-app-minivm=y \
		$(ASTPKG)-app-originate=y $(ASTPKG)-app-read=y $(ASTPKG)-app-readexten=y $(ASTPKG)-app-sayunixtime=y \
		$(ASTPKG)-app-stack=y $(ASTPKG)-app-system=y \
		$(ASTPKG)-app-talkdetect=y $(ASTPKG)-app-verbose=y $(ASTPKG)-app-waituntil=y $(ASTPKG)-app-while=y \
		$(ASTPKG)-chan-iax2=y $(ASTPKG)-chan-dongle=y $(ASTPKG)-codec-a-mu=y \
		$(ASTPKG)-codec-alaw=y $(ASTPKG)-codec-g722=y $(ASTPKG)-codec-g726=y $(ASTPKG)-codec-gsm=y \
		$(ASTPKG)-curl=y \
		$(ASTPKG)-format-g726=y $(ASTPKG)-format-gsm=y \
		$(ASTPKG)-format-g729=y $(ASTPKG)-format-sln=y $(ASTPKG)-func-blacklist=y \
		$(ASTPKG)-func-channel=y $(ASTPKG)-func-cut=y $(ASTPKG)-func-db=y $(ASTPKG)-func-devstate=y $(ASTPKG)-func-extstate=y \
		$(ASTPKG)-func-global=y $(ASTPKG)-func-shell=y $(ASTPKG)-func-uri=y $(ASTPKG)-func-vmcount=y \
		$(ASTPKG)-pbx-ael=y $(ASTPKG)-pbx-spool=y $(ASTPKG)-res-ael-share=y $(ASTPKG)-res-agi=y \
		$(ASTPKG)-res-musiconhold=y $(ASTPKG)-cdr=y $(ASTPKG)-sounds=y $(ASTPKG)-voicemail=y $(ASTPKG)-cdr-sqlite3=y $(ASTPKG)-cdr-csv=y \
		$(ASTPKG)-chan-ooh323=y
else
	BEESIP_PACKAGES+=\
		$(ASTPKG)-gui=y \
		$(ASTPKG)-app-chanisavail=y $(ASTPKG)-app-chanspy=y $(ASTPKG)-app-exec=y $(ASTPKG)-app-minivm=y \
		$(ASTPKG)-app-originate=y $(ASTPKG)-app-read=y $(ASTPKG)-app-readexten=y $(ASTPKG)-app-sayunixtime=y \
		$(ASTPKG)-app-stack=y $(ASTPKG)-app-system=y \
		$(ASTPKG)-app-talkdetect=y $(ASTPKG)-app-verbose=y $(ASTPKG)-app-waituntil=y $(ASTPKG)-app-while=y \
		$(ASTPKG)-chan-iax2=y $(ASTPKG)-chan-dongle=y $(ASTPKG)-codec-a-mu=y \
		$(ASTPKG)-codec-alaw=y $(ASTPKG)-codec-g722=y $(ASTPKG)-codec-g726=y $(ASTPKG)-codec-gsm=y \
		$(ASTPKG)-curl=y \
		$(ASTPKG)-format-g726=y $(ASTPKG)-format-gsm=y \
		$(ASTPKG)-format-g729=y $(ASTPKG)-format-sln=y $(ASTPKG)-func-blacklist=y \
		$(ASTPKG)-func-channel=y $(ASTPKG)-func-cut=y $(ASTPKG)-func-db=y $(ASTPKG)-func-devstate=y $(ASTPKG)-func-extstate=y \
		$(ASTPKG)-func-global=y $(ASTPKG)-func-shell=y $(ASTPKG)-func-uri=y $(ASTPKG)-func-vmcount=y \
		$(ASTPKG)-pbx-ael=y $(ASTPKG)-pbx-spool=y $(ASTPKG)-res-ael-share=y $(ASTPKG)-res-agi=y \
		$(ASTPKG)-res-musiconhold=y $(ASTPKG)-cdr=y $(ASTPKG)-sounds=y $(ASTPKG)-voicemail=y $(ASTPKG)-cdr-sqlite3=y $(ASTPKG)-cdr-csv=y \
		$(ASTPKG)-chan-ooh323=y
endif

BEESIP_PACKAGES+=\
		$(KAMPKG)=y $(KAMPKG)-mod-corex=y $(KAMPKG)-mod-auth=y $(KAMPKG)-mod-cfg-rpc=y $(KAMPKG)-mod-cfgutils=y $(KAMPKG)-mod-ctl=y $(KAMPKG)-mod-db-sqlite=y $(KAMPKG)-mod-exec=y \
		$(KAMPKG)-mod-enum=y $(KAMPKG)-mod-ipops=y $(KAMPKG)-mod-dialog=y $(KAMPKG)-mod-nathelper=y $(KAMPKG)-mod-pike=y $(KAMPKG)-mod-rr=y $(KAMPKG)-mod-tm=y $(KAMPKG)-mod-usrloc=y $(KAMPKG)-mod-rtpproxy=y \
		$(KAMPKG)-mod-sipcapture=y $(KAMPKG)-mod-siptrace=y \
		$(KAMPKG)-mod-auth-db=y $(KAMPKG)-mod-avpops=y $(KAMPKG)-mod-benchmark=y $(KAMPKG)-mod-cfg-db=y \
		$(KAMPKG)-mod-db-mysql=y $(KAMPKG)-mod-db-unixodbc=y $(KAMPKG)-mod-dispatcher=y $(KAMPKG)-mod-domainpolicy=y \
		$(KAMPKG)-mod-h350=y $(KAMPKG)-mod-ldap=y $(KAMPKG)-mod-pdt=y $(KAMPKG)-mod-permissions=y \
		$(KAMPKG)-mod-presence-dialoginfo=y $(KAMPKG)-mod-presence-mwi=y $(KAMPKG)-mod-presence-xml=y \
		$(KAMPKG)-mod-sms=y $(KAMPKG)-mod-speeddial=y $(KAMPKG)-mod-sqlops=y $(KAMPKG)-mod-sst=y \
		$(KAMPKG)-mod-statistics=y $(KAMPKG)-mod-uac=y $(KAMPKG)-mod-uac-redirect=y $(KAMPKG)-mod-uri-db=y \
		php5=y php5-cgi=y php5-cli=y php5-fastcgi=y php5-mod-ctype=y \
		php5-mod-curl=y php5-mod-dom=y php5-mod-exif=y php5-mod-fileinfo=y php5-mod-ftp=y \
		php5-mod-gmp=y php5-mod-hash=y php5-mod-iconv=y php5-mod-json=y \
		php5-mod-ldap=y php5-mod-mbstring=y php5-mod-mcrypt=y php5-mod-openssl=y \
		php5-mod-pcntl=y php5-mod-pdo=y php5-mod-pdo-sqlite=y php5-mod-session=y \
		php5-mod-simplexml=y php5-mod-soap=y php5-mod-sockets=y \
		php5-mod-sqlite3=y php5-mod-sysvmsg=y php5-mod-sysvsem=y php5-mod-sysvshm=y \
		php5-mod-tokenizer=y php5-mod-xml=y php5-mod-xmlreader=y php5-mod-xmlwriter=y \
		unixodbc-tools=y \
		bzip2=y ntpdate=y \
		jamvm=y classpath=y \
		ip=y \
		openssh-client-utils=y openssh-keygen=y openssh-server=y openssh-client=y rsync=y tcpdump=y

UCIPROV_UCIDEFAULTS+=system.@system[0].hostname=Beesip-PBX
UCIPROV_OPKG+="update" "install uciprov"

OWRT_ADDED_FILES+=$(call AddDirFiles,pbx-files/www) $(call AddDirFiles,pbx-files/etc)

OWRT_ADDED_FILES+=\
        pbx-files/etc/inittab:/etc/inittab \
        pbx-files/etc/profile:/etc/profile \
        pbx-files/etc/banner:/etc/banner \
        pbx-files/etc/ssh/sshd_config:/etc/ssh/sshd_config \
        pbx-files/lib/callmon/functions.sh:/lib/callmon/functions.sh \
        pbx-files/etc/config/system:/etc/config/system \
        pbx-files/dhcp_disable.uci:/etc/config/dhcp

OWRT_CONFIG_SET += \
        BUSYBOX_CONFIG_LOGIN=y \
        BUSYBOX_CONFIG_LOGIN_SESSION_AS_CHILD=y \
        BUSYBOX_CONFIG_LOGIN_SCRIPTS=y \
        BUSYBOX_CONFIG_FEATURE_NOLOGIN=y \
        BUSYBOX_CONFIG_FEATURE_SECURETTY=y \
        BUSYBOX_CONFIG_FEATURE_EDITING_HISTORY=256 \
        BUSYBOX_CONFIG_FEATURE_EDITING_SAVEHISTORY=y \
        BUSYBOX_CUSTOM=y

OWRT_CONFIG_UNSET += \
        CONFIG_DEFAULT_dropbear \
        CONFIG_PACKAGE_dropbear
