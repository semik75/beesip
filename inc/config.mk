ARCH=$(shell uname -m)
DEPS_x86:= build-essential asciidoc binutils bzip2 gawk gettext git libncurses5-dev libz-dev patch unzip zlib1g-dev flex quilt scons libxml-parser-perl bison libgmp3-dev antlr openjdk-6-jdk openjdk-6-jre mtd-tools python-dev zip qemu-user-static binfmt-support sudo texinfo
DEPS_x86_64:= build-essential asciidoc binutils bzip2 gawk gettext git libncurses5-dev libz-dev patch unzip zlib1g-dev ia32-libs lib32gcc1 libc6-dev-i386 flex quilt scons libxml-parser-perl bison libgmp3-dev antlr openjdk-6-jdk openjdk-6-jre mtd-tools python-dev zip qemu-user-static binfmt-support sudo texinfo

CP=cp --reflink=auto
RM=rm

BUILD_DIR=$(PWD)/build
DL_DIR=$(PWD)/build/dl
JOBS=6
MAKE_ARGS=
BEESIP_GIT_REV=$(shell git rev-parse HEAD)
MAKE_ARGS += BEESIP_GIT_REV=$(BEESIP_GIT_REV)

BEESIP_PACKAGES_DIR=$(BEESIP_IMG_DIR)/

BEESIP_RSYNC_MIRROR=

OWRT_CONFIG_SET = \
 DEVEL=y \
 LOCALMIRROR=\"http://suzelly.opf.slu.cz/~sla463/beesip/mirror/dl/\" \
 DOWNLOAD_FOLDER=\"$(DL_DIR)\" \
 BROKEN=y \
 IMAGEOPT=y \
 VERSIONOPT=y \
 PREINITOPT=y \
 INITOPT=y \
 VERSION_DIST=\"Beesip\" \
 LIBCURL_TFTP=y LIBCURL_HTTP=y LIBCURL_FTP=y LIBCURL_FILE=y LIBCURL_OPENSSL=y

OWRT_CONFIG_UNSET+=AUTOREBUILD

OWRT_PATCHES:=owrtshell.diff

VIRTFS_SECURITY:=security_model=mapped-file
VIRTFS_OPTIONS:=trans=virtio,version=9p2000.L

