# Post configuration script for BEESIP
# All config and target variables has to be set before including this!

ifneq ($(DBG),)
  $(eval $(call BeesipDefaults,dbg))
endif

ifneq ($(VERBOSE),)
  JOBS = 1
  MAKE_ARGS += V=99
endif

# Defaults
REPO_DIR=$(PWD)/build/repo

ifeq ($(OWRT_DIR),)
  OWRT_DIR=$(BUILD_DIR)/owrt-$(TARGET_CPU)-$(OWRT_NAME)$(OWRT_SUFFIX)$(TARGET_CPU_SUFFIX)
endif

# Eglibc is broken now. Ignore any errors during compiling packages and try to be lucky ...
ifneq ($(EGLIBC),)
	MAKEFORCEI=-i
endif

OWRT_GIT_REV=$(shell if [ -d "$(OWRT_DIR)" ]; then (cd $(OWRT_DIR) && git rev-parse HEAD); fi)
MAKE_ARGS += OWRT_GIT_REV=$(OWRT_GIT_REV)
ifeq ($(OWRT_NAME),trunk)
  OWRT_CONFIG_SET += BUILD_LOG=y
  TRUNK_GITREV+= $(OWRT_GIT_REV)
 ifneq ($(DBG),)
    TARGET_SUFFIX+=_r$(OWRT_GIT_REV)-dbg
 else
  ifeq ($(BEESIP_VERSION),trunk)
    TARGET_SUFFIX+=_r$(OWRT_GIT_REV)
  endif
 endif
endif
MAKE_ARGS += \
  OWRT_VERSION=$(OWRT_NAME) \
  BEESIP_VERSION=$(BEESIP_VERSION) \
  BEESIP_PACKAGES="$(BEESIP_PACKAGES)" \
  EMBEDDED_MODULES="$(EMBEDDED_MODULES)" \
  OWRT_ADDED_ETCDEFAULT="$(OWRT_ADDED_ETCDEFAULT)" \
  OWRT_ADDED_FILES="$(OWRT_ADDED_FILES)"

IMG_DIR=$(OWRT_DIR)/bin/$(TARGET_CPU)$(TARGET_CPU_SUFFIX)
BEESIP_IMG_DIR=$(REPO_DIR)/$(BEESIP_VERSION)/owrt_$(OWRT_NAME)/$(TARGET_CPU)$(CPU_SUFFIX)
BEESIP_PKG_DIR=$(BEESIP_IMG_DIR)/packages
OWRT_PKG_DIR=$(IMG_DIR)/packages
BEESIP_IMG_PREFIX=beesip-$(TARGET_NAME)$(TARGET_SUFFIX)

ifeq ($(SUBTARGET),)
 SUBTARGET=generic
endif

#ImageBuilder
PKG_OS:=$(shell uname -s)
PKG_CPU:=$(word 1,$(subst -, ,$(shell $(CC) -dumpmachine)))
ifneq ($(OWRT_IMG_PROFILE),)
 IB_NAME:=OpenWrt-ImageBuilder-$(BEESIP_VERSION)-$(TARGET_CPU)-$(SUBTARGET).$(PKG_OS)-$(PKG_CPU)
 OWRT_CONFIG_SET += IB=y
endif

ifneq ($(OWRT_IMG_SQUASHFS_NAME),)
	OWRT_IMG_SQUASHFS=$(IMG_DIR)/$(OWRT_IMG_SQUASHFS_NAME)
	BEESIP_IMG_SQUASHFS=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX).squashfs
endif

ifneq ($(OWRT_IMG_DISK_NAME),)
	OWRT_IMG_DISK=$(IMG_DIR)/$(OWRT_IMG_DISK_NAME)
	BEESIP_IMG_VMDK=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX).vmdk
	BEESIP_IMG_VMDK_FLAT=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX)-flat.vmdk
	BEESIP_IMG_DISK=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX).img
	BEESIP_VMX=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX).vmx
	BEESIP_DATA_VMDK=$(BEESIP_IMG_DIR)/beesip-data.vmdk
	BEESIP_DATA_VMDK_FLAT=$(BEESIP_IMG_DIR)/beesip-data-flat.vmdk
	BEESIP_KVMSH=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX).kvm.sh
endif

ifneq ($(OWRT_IMG_KERNEL_NAME),)
	OWRT_IMG_KERNEL=$(IMG_DIR)/$(OWRT_IMG_KERNEL_NAME)
	BEESIP_IMG_KERNEL=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX).vmlinuz
endif

ifneq ($(OWRT_IMG_FACTORY_NAME),)
	OWRT_IMG_FACTORY=$(IMG_DIR)/$(OWRT_IMG_FACTORY_NAME)
	BEESIP_IMG_FACTORY=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX)-factory.bin
endif

ifneq ($(OWRT_IMG_BIN_NAME),)
	OWRT_IMG_BIN=$(IMG_DIR)/$(OWRT_IMG_BIN_NAME)
	BEESIP_IMG_BIN=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX)-sysupgrade.bin
endif

ifneq ($(OWRT_IMG_VMDK_NAME),)
	OWRT_IMG_VMDK=$(IMG_DIR)/$(OWRT_IMG_VMDK_NAME)
	BEESIP_IMG_VMDK=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX).vmdk
	BEESIP_VMX=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX).vmx
	BEESIP_DATA_VMDK=$(BEESIP_IMG_DIR)/beesip-data.vmdk
endif

ifneq ($(IMG_OVA),)
	BEESIP_IMG_OVA=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX).ova
	BEESIP_IMG_OVF=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX).ovf
endif

ifneq ($(OWRT_IMG_ISO_NAME),)
	OWRT_IMG_ISO=$(IMG_DIR)/$(OWRT_IMG_ISO_NAME)
	BEESIP_IMG_ISO=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX).iso
endif

ifeq ($(OWRT_IMG_SYSUPGRADE_NAME),)
 OWRT_IMG_SYSUPGRADE=$(IMG_DIR)/$(OWRT_IMG_BIN_NAME)
else
 OWRT_IMG_SYSUPGRADE=$(IMG_DIR)/$(OWRT_IMG_SYSUPGRADE_NAME)
endif
BEESIP_IMG_SYSUPGRADE=$(BEESIP_IMG_DIR)/$(BEESIP_IMG_PREFIX).sysupgrade

OWRT_IMAGES=$(OWRT_IMG_BIN) $(OWRT_IMG_KERNEL) $(OWRT_IMG_DISK) $(OWRT_IMG_SQUASHFS) $(OWRT_IMG_VMDK) $(OWRT_IMG_TRX) $(OWRT_IMG_ISO)
BEESIP_IMAGES=$(BEESIP_IMG_BIN) $(BEESIP_IMG_KERNEL) $(BEESIP_IMG_DISK) $(BEESIP_IMG_SQUASHFS) $(BEESIP_IMG_VMDK) $(BEESIP_IMG_FACTORY) $(BEESIP_IMG_ISO) $(BEESIP_IMG_SYSUPGRADE) $(BEESIP_IMG_OVA)

# Load defaults for specific owrt version
$(eval $(call BeesipDefaults,owrt-$(OWRT_NAME)))
# Load defaults for beesip version
$(eval $(call BeesipDefaults,$(BEESIP_VERSION)))
# Load defaults for target CPU
$(eval $(call BeesipDefaults,$(TARGET_CPU)))

ifeq ($(REPOURL),)
 REPOURL=http://mirror.opf.slu.cz/beesip/
endif

OWRT_CONFIG_SET += VERSION_NUMBER=\"$(BEESIP_VERSION)\" VERSION_NICK=\"$(OWRT_NAME)\" VERSION_PRODUCT=\"$(BEESIP_IMG_PREFIX)\" VERSION_REPO=\"$(REPOURL)/$(BEESIP_VERSION)/owrt_$(OWRT_NAME)/$(TARGET_CPU)/packages\"

LOG_DIR=$(PWD)/log/$(TARGET_NAME)$(TARGET_SUFFIX)
KVM_DIR=$(OWRT_DIR)/testkvm/
OWRT_SHELL=$(OWRT_DIR)/scripts/shell.sh

BMAKE=make $(MAKE_ARGS)

# Construct default beesip packages
ifeq ($(BEESIPPKG),y)
 BEESIP_DPACKAGES += beesip=y
endif
# Construct beesip packages from defaults and overrided packages
BEESIP_PACKAGES := $(BEESIP_DPACKAGES) $(BEESIP_PACKAGES)

KVM_DHCPOPTS=tftp=$(KVM_TFTP_DIR),bootfile=$(KVM_BOOT_FILE),dnssearch=$(KVM_DNSSEARCH),hostname=$(KVM_HOSTNAME)
KVM_NICOPTS=-net nic,model=e1000 -net user,hostfwd=tcp::2222-:22,hostfwd=tcp::2223-:23,hostfwd=tcp::2280-:80,hostfwd=tcp::2281-:8088,$(KVM_DHCPOPTS) -net nic,model=e1000
KVM_TFTP_DIR="$(KVM_DIR)/tftp"
KVM_TFTP_TMP="$(KVM_DIR)/tftp/tmp"
QEMU=qemu-system-$(TARGET_QEMU) $(TARGET_QEMU_OPTS)
KVM=qemu-system-$(TARGET_QEMU) $(KVM_NICOPTS) $(TARGET_QEMU_OPTS)

$(eval $(call BeesipDefaults,uciprov))

BEESIP_PACKAGES:=$(subst ___,\",$(shell ./scripts/rmdup.sh BEESIP_PACKAGES $(subst ",___,$(BEESIP_PACKAGES))))
EMBEDDED_MODULES:=$(subst ___,\",$(shell ./scripts/rmdup.sh EMBEDDED_MODULES $(subst ",___,$(EMBEDDED_MODULES))))
OWRT_CONFIG_SET:=$(subst ___,\",$(shell ./scripts/rmdup.sh OWRT_CONFIG_SET $(subst ",___,$(OWRT_CONFIG_SET))))
OWRT_CONFIG_UNSET:=$(subst ___,\",$(shell ./scripts/rmdup.sh OWRT_CONFIG_UNSET $(subst ",___,$(OWRT_CONFIG_UNSET))))

# List of packages embeded into img
BEESIP_EPACKAGES=$(foreach pkg,$(filter %=y,$(BEESIP_PACKAGES)),$(subst =y,,$(pkg))) $(foreach pkg,$(filter %=n,$(BEESIP_PACKAGES)),$(subst =n,,-$(pkg))) $(foreach pkg,$(filter %=m,$(BEESIP_PACKAGES)),$(subst =m,,-$(pkg)))

# Stamps:
STAMP_DIR=$(OWRT_DIR).stamp
OSTAMP_DIR=$(OWRT_DIR)/stamp
SPREREQ=$(STAMP_DIR)/prereq
SGITCLONE=$(OSTAMP_DIR)/gitclone
SFEEDSU=$(OSTAMP_DIR)/feeds-updated
SFEEDSI=$(OSTAMP_DIR)/feeds-installed
SPATCHED=$(OSTAMP_DIR)/owrt-patched
SOWRTPREP=$(OSTAMP_DIR)/owrt-prepared
SKERNPREP=$(STAMP_DIR)/kernel-prepared
SOWRTCFG=$(STAMP_DIR)/owrt-configured
SOWRTIMG=$(STAMP_DIR)/owrt-image-created
SOWRTIMGI=$(STAMP_DIR)/owrt-image-premaked
SOWRTIMGB=$(OSTAMP_DIR)/owrt-imagebuilder
OWRTTCPREP=$(STAMP_DIR)/owrt-toolchain-installed

