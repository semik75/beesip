
OWRT_GIT="git://git.openwrt.org/openwrt.git"

USE_CUSTOM_FEEDS=1

$(eval $(call AddDefaultOpenWrtFeed,src-git packages https://github.com/openwrt/packages.git))
$(eval $(call AddDefaultOpenWrtFeed,src-git luci https://github.com/openwrt/luci.git))
$(eval $(call AddDefaultOpenWrtFeed,src-git routing https://github.com/openwrt-routing/packages.git))
$(eval $(call AddDefaultOpenWrtFeed,src-git telephony https://github.com/openwrt/telephony.git))
$(eval $(call AddDefaultOpenWrtFeed,src-git management https://github.com/openwrt-management/packages.git))
$(eval $(call AddDefaultOpenWrtFeed,src-git targets https://github.com/openwrt/targets.git))
