BEESIP_NODEFAULTS=files pbx sbc callmon

BEESIP_PACKAGES += \
	openssh-client-utils=y openssh-keygen=y openssh-server=y openssh-client=y \
        ip-full=y vim=y nano=y mc=y rsync=y python-pip=y curl=y \
	uciprov=y sqlite3-cli=y \
	openssl-util=y luci=y wget=y kmod-ipv6=y kmod-ip6tables=y ip6tables=y \
	kamailio4=y kamailio4-mod-corex=y kamailio4-mod-auth=y kamailio4-mod-auth-db=y kamailio4-mod-cfg-rpc=y kamailio4-mod-cfgutils=y kamailio4-mod-ctl=y kamailio4-mod-db-sqlite=y kamailio4-mod-exec=y \
	kamailio4-mod-enum=y kamailio4-mod-ipops=y kamailio4-mod-dialog=y kamailio4-mod-nathelper=y kamailio4-mod-pike=y kamailio4-mod-rr=y kamailio4-mod-tm=y kamailio4-mod-usrloc=y kamailio4-mod-rtpproxy=y \
	kamailio4-mod-sipcapture=y kamailio4-mod-siptrace=y kamailio4-mod-permissions=y \
	kamailio4-mod-acc=y kamailio4-mod-alias-db=y kamailio4-mod-avpops=y \
	kamailio4-mod-db-flatstore=y kamailio4-mod-ldap=y \
	kamailio4-mod-db-text=y kamailio4-mod-dialplan=y \
	kamailio4-mod-diversion=y kamailio4-mod-domain=y kamailio4-mod-group=y \
	kamailio4-mod-htable=y kamailio4-mod-kex=y kamailio4-mod-lcr=y kamailio4-mod-maxfwd=y \
	kamailio4-mod-mediaproxy=y kamailio4-mod-mi-datagram=y kamailio4-mod-mi-fifo=y kamailio4-mod-mi-rpc=y \
	kamailio4-mod-msilo=y kamailio4-mod-nat-traversal=y kamailio4-mod-drouting=y kamailio4-mod-xmlrpc=y \
	kamailio4-mod-path=y kamailio4-mod-presence=y \
	kamailio4-mod-pv=y kamailio4-mod-qos=y kamailio4-mod-ratelimit=y kamailio4-mod-regex=y kamailio4-mod-registrar=y \
	kamailio4-mod-rls=y kamailio4-mod-rtimer=y kamailio4-mod-sanity=y \
	kamailio4-mod-siputils=y kamailio4-mod-sl=y kamailio4-mod-textops=y kamailio4-mod-tls=y \
	kamailio4-mod-tmx=y kamailio4-mod-userblacklist=y kamailio4-mod-utils=y \
	kamailio4-mod-xcap-client=y kamailio4-mod-xlog=y kamailio4-mod-xmpp=y \
	libssh2=y \
	coreutils=y coreutils-nohup=y \
	unixodbc=y \
	luci-ssl=y \
	kmod-fs-vfat=y \
        asterisk11=y asterisk11-app-alarmreceiver=y asterisk11-app-authenticate=y \
        asterisk11-app-chanisavail=y asterisk11-app-chanspy=y asterisk11-app-confbridge=y \
        asterisk11-app-directed_pickup=y asterisk11-app-disa=y asterisk11-app-exec=y \
        asterisk11-app-minivm=y asterisk11-app-mixmonitor=y asterisk11-app-originate=y \
        asterisk11-app-playtones=y asterisk11-app-read=y asterisk11-app-readexten=y \
        asterisk11-app-record=y asterisk11-app-sayunixtime=y asterisk11-app-senddtmf=y \
        asterisk11-app-sms=y asterisk11-app-stack=y asterisk11-app-system=y \
        asterisk11-app-talkdetect=y asterisk11-app-verbose=y asterisk11-app-waituntil=y \
        asterisk11-app-while=y \
        asterisk11-chan-agent=y asterisk11-chan-iax2=y \
        asterisk11-chan-mgcp=y asterisk11-chan-ooh323=y \
        asterisk11-chan-skinny=y asterisk11-chan-unistim=y asterisk11-codec-a-mu=y \
        asterisk11-codec-adpcm=y asterisk11-codec-alaw=y asterisk11-codec-g722=y \
        asterisk11-codec-g726=y asterisk11-codec-gsm=y \
        asterisk11-codec-ilbc=y asterisk11-codec-lpc10=y asterisk11-codec-resample=y \
        asterisk11-curl=y asterisk11-format-g726=y asterisk11-format-g729=y \
        asterisk11-format-gsm=y asterisk11-format-h263=y asterisk11-format-h264=y \
        asterisk11-format-ilbc=y asterisk11-format-sln=y asterisk11-format-vox=y \
        asterisk11-format-wav=y asterisk11-format-wav-gsm=y asterisk11-func-base64=y \
        asterisk11-func-blacklist=y asterisk11-func-channel=y asterisk11-func-cut=y \
        asterisk11-func-db=y asterisk11-func-devstate=y asterisk11-func-enum=y \
        asterisk11-func-env=y asterisk11-func-extstate=y asterisk11-func-global=y \
        asterisk11-func-groupcount=y asterisk11-func-math=y asterisk11-func-module=y \
        asterisk11-func-shell=y asterisk11-func-uri=y asterisk11-func-vmcount=y \
        asterisk11-odbc=y asterisk11-pbx-ael=y asterisk11-pbx-dundi=y asterisk11-pbx-lua=y \
        asterisk11-pbx-spool=y asterisk11-res-ael-share=y asterisk11-res-agi=y \
        asterisk11-res-clioriginate=y \
        asterisk11-res-monitor=y asterisk11-res-musiconhold=y asterisk11-res-phoneprov=y \
        asterisk11-res-pktccops=y asterisk11-res-smdi=y asterisk11-res-srtp=y \
        asterisk11-res-timing-pthread=y asterisk11-res-timing-timerfd=y asterisk11-sounds=y asterisk11-voicemail=y

OWRT_ADDED_FILES=$(call AddDirFiles,sbc-files)

OWRT_CONFIG_SET += \
        BUSYBOX_CONFIG_LOGIN=y \
        BUSYBOX_CONFIG_LOGIN_SESSION_AS_CHILD=y \
        BUSYBOX_CONFIG_LOGIN_SCRIPTS=y \
        BUSYBOX_CONFIG_FEATURE_NOLOGIN=y \
        BUSYBOX_CONFIG_FEATURE_SECURETTY=y \
        BUSYBOX_CONFIG_FEATURE_EDITING_HISTORY=256 \
        BUSYBOX_CONFIG_FEATURE_EDITING_SAVEHISTORY=y \
        BUSYBOX_CUSTOM=y

OWRT_ADDED_FILES+=\
        sbc-files/etc/inittab:/etc/inittab \
        sbc-files/etc/profile:/etc/profile \
        sbc-files/etc/banner:/etc/banner \
        sbc-files/etc/ssh/sshd_config:/etc/ssh/sshd_config \
        sbc-files/etc/config/system:/etc/config/system \
        sbc-files/dhcp_disable.uci:/etc/config/dhcp

OWRT_CONFIG_UNSET += \
        CONFIG_DEFAULT_dropbear \
        CONFIG_PACKAGE_dropbear

UCIPROV_UCIDEFAULTS= system.@system[0].hostname=Beesip-SBC
UCIPROV_OPKG= "update" "install uciprov"

