BEESIP_NODEFAULTS=generic files callmon sbc pbx

BEESIP_PACKAGES+=\
	iperf=y iperf3=y kmod-netem=y \
	openssh-client-utils=y openssh-keygen=y	openssh-server=y openssh-client=y rsync=y \
	ip-full=y vim=y nano=y mc=y python-pip=y uciprov=y curl=y tcpdump=y bkprsender=y \
	asterisk13=y asterisk13-app-alarmreceiver=y asterisk13-app-authenticate=y \
	asterisk13-app-chanisavail=y asterisk13-app-chanspy=y asterisk13-app-confbridge=y \
	asterisk13-app-directed_pickup=y asterisk13-app-disa=y asterisk13-app-exec=y \
	asterisk13-app-minivm=y asterisk13-app-mixmonitor=y asterisk13-app-originate=y \
	asterisk13-app-playtones=y asterisk13-app-read=y asterisk13-app-readexten=y \
	asterisk13-app-record=y asterisk13-app-sayunixtime=y asterisk13-app-senddtmf=y \
	asterisk13-app-sms=y asterisk13-app-stack=y asterisk13-app-system=y \
	asterisk13-app-talkdetect=y asterisk13-app-verbose=y asterisk13-app-waituntil=y \
	asterisk13-app-while=y asterisk13-chan-iax2=y asterisk13-chan-sip=y asterisk13-pjsip=y \
	asterisk13-codec-a-mu=y \
	asterisk13-codec-adpcm=y asterisk13-codec-alaw=y asterisk13-codec-g722=y \
	asterisk13-codec-g726=y asterisk13-codec-gsm=y \
	asterisk13-codec-ilbc=y asterisk13-codec-lpc10=y asterisk13-codec-resample=y \
	asterisk13-curl=y asterisk13-format-g726=y asterisk13-format-g729=y \
	asterisk13-format-gsm=y asterisk13-format-h263=y asterisk13-format-h264=y \
	asterisk13-format-ilbc=y asterisk13-format-sln=y asterisk13-format-vox=y \
	asterisk13-format-wav=y asterisk13-format-wav-gsm=y asterisk13-func-base64=y \
	asterisk13-func-blacklist=y asterisk13-func-channel=y asterisk13-func-cut=y \
	asterisk13-func-db=y asterisk13-func-devstate=y asterisk13-func-enum=y \
	asterisk13-func-env=y asterisk13-func-extstate=y asterisk13-func-global=y \
	asterisk13-func-groupcount=y asterisk13-func-math=y asterisk13-func-module=y \
	asterisk13-func-shell=y asterisk13-func-uri=y asterisk13-func-vmcount=y \
	asterisk13-odbc=y asterisk13-pbx-ael=y asterisk13-pbx-dundi=y \
	asterisk13-pbx-spool=y asterisk13-res-ael-share=y asterisk13-res-agi=y \
	asterisk13-res-clioriginate=y asterisk13-res-hep=y asterisk13-res-hep-rtcp=y asterisk13-res-hep-pjsip=y \
	asterisk13-res-monitor=y asterisk13-res-musiconhold=y asterisk13-res-phoneprov=y \
	asterisk13-res-smdi=y asterisk13-res-srtp=y \
	asterisk13-res-timing-pthread=y asterisk13-res-timing-timerfd=y asterisk13-sounds=y asterisk13-voicemail=y \
        openssl-util=y luci=y wget=y kmod-ipv6=y kmod-ip6tables=y ip6tables=y \
        kamailio4=y kamailio4-mod-corex=y kamailio4-mod-auth=y kamailio4-mod-cfg-rpc=y kamailio4-mod-cfgutils=y kamailio4-mod-ctl=y kamailio4-mod-db-sqlite=y kamailio4-mod-exec=y \
        kamailio4-mod-enum=y kamailio4-mod-ipops=y kamailio4-mod-dialog=y kamailio4-mod-nathelper=y kamailio4-mod-pike=y kamailio4-mod-rr=y kamailio4-mod-tm=y kamailio4-mod-usrloc=y kamailio4-mod-rtpproxy=y \
        kamailio4-mod-sipcapture=y kamailio4-mod-siptrace=y \
        kamailio4-mod-acc=y =y kamailio4-mod-alias-db=y \
        kamailio4-mod-db-flatstore=y \
        kamailio4-mod-db-text=y kamailio4-mod-dialplan=y \
        kamailio4-mod-diversion=y kamailio4-mod-domain=y kamailio4-mod-group=y \
        kamailio4-mod-htable=y kamailio4-mod-kex=y kamailio4-mod-lcr=y kamailio4-mod-maxfwd=y \
        kamailio4-mod-mediaproxy=y kamailio4-mod-mi-datagram=y kamailio4-mod-mi-fifo=y kamailio4-mod-mi-rpc=y \
        kamailio4-mod-msilo=y kamailio4-mod-nat-traversal=y kamailio4-mod-drouting=y kamailio4-mod-xmlrpc=y \
        kamailio4-mod-path=y kamailio4-mod-presence=y \
        kamailio4-mod-pv=y kamailio4-mod-qos=y kamailio4-mod-ratelimit=y kamailio4-mod-regex=y kamailio4-mod-registrar=y \
        kamailio4-mod-rls=y kamailio4-mod-rtimer=y kamailio4-mod-sanity=y \
        kamailio4-mod-siputils=y kamailio4-mod-sl=y kamailio4-mod-textops=y kamailio4-mod-tls=y \
        kamailio4-mod-tmx=y kamailio4-mod-userblacklist=y kamailio4-mod-utils=y \
        kamailio4-mod-xcap-client=y kamailio4-mod-xlog=y kamailio4-mod-xmpp=y

OWRT_ADDED_FILES+=\
	honey-files/etc/profile:/etc/profile \
	honey-files/etc/banner:/etc/banner \
	honey-files/etc/inittab:/etc/inittab \
	honey-files/etc/ssh/sshd_config:/etc/ssh/sshd_config \
	honey-files/lib/beesip/functions.sh:/lib/beesip/functions.sh \
	honey-files/etc/config/system:/etc/config/system \
	honey-files/dhcp_disable.uci:/etc/config/dhcp

OWRT_CONFIG_UNSET += \
	CONFIG_DEFAULT_dropbear \
	CONFIG_PACKAGE_dropbear

OWRT_CONFIG_SET += \
        BUSYBOX_CONFIG_LOGIN=y \
        BUSYBOX_CONFIG_LOGIN_SESSION_AS_CHILD=y \
        BUSYBOX_CONFIG_LOGIN_SCRIPTS=y \
        BUSYBOX_CONFIG_FEATURE_NOLOGIN=y \
        BUSYBOX_CONFIG_FEATURE_SECURETTY=y \
        BUSYBOX_CONFIG_FEATURE_EDITING_HISTORY=256 \
        BUSYBOX_CONFIG_FEATURE_EDITING_SAVEHISTORY=y \
        BUSYBOX_CUSTOM=y
