BEESIP_NODEFAULTS=uciprov files

ifeq ($(OWRT_NAME),chaos_calmer)
	ZABBIXPKG=zabbix3
else
	ZABBIXPKG=zabbix
endif

BEESIP_PACKAGES += luci=m uciprov=y kmod-ipv6=y \
  $(ZABBIXPKG)-agentd=y $(ZABBIXPKG)-extra-mac80211=y $(ZABBIXPKG)-extra-network=y $(ZABBIXPKG)-extra-wifi=y \
  unbound-host=m ppp=m kmod-pppoe=m ppp-mod-pppoe=m kmod-ppp=y kmod-tun=y \
  wpad=y wpad-mini=n haveged=y lldpd=y \
  openvpn-openssl=y tcpdump=y iftop=y ip=y \
  radsecproxy=y eapol-test=y curl=y \
  kmod-cfg80211=y kmod-mac80211=y iw=y openssl-util=y 

OWRT_PATCHES += radsecproxy-fticks.patch

OWRT_ADDED_FILES += $(call AddDirFiles,eduroamap-files)

OWRT_IMG_BIN_NAME=openwrt-$(OWRT_NAME)-$(TARGET_CPU)-$(HWSUFFIX)-squashfs-sysupgrade.bin
OWRT_IMG_SYSUPGRADE_NAME=openwrt-$(OWRT_NAME)-$(TARGET_CPU)-$(HWSUFFIX)-squashfs-sysupgrade.bin
OWRT_IMG_FACTORY_NAME=openwrt-$(OWRT_NAME)-$(TARGET_CPU)-$(HWSUFFIX)-squashfs-factory.bin
OWRT_IMG_KERNEL_NAME=openwrt-$(OWRT_NAME)-$(TARGET_CPU)-generic-vmlinux.elf

# Uciprov config
OWRT_CONFIG_SET += \
	UCIPROV_USE_DNS=y \
	UCIPROV_USE_DNSSEC=y \
	UCIPROV_USE_DHCP=y \
	UCIPROV_USE_STATIC=y \
	UCIPROV_URI1="http://{hostname}:{boxid}@eduroamap.{domain}/default/{cn}/{m}.{fde}" \
	UCIPROV_URI2="http://{hostname}:{boxid}@eduroamap.{domain}/{board}/{cn}/{m}.{fde}" \
	UCIPROV_URI3="http://{hostname}:{boxid}@eduroamap.{domain}/{cn}/{mac}/{m}.{fde}" \
	UCIPROV_DNS_PREFIX="eduroamap" \
	UCIPROV_INTERFACE="wan" \
	UCIPROV_TGZ=y \
	UCIPROV_SU=y \
	UCIPROV_RECOVERY=y \
	UCIPROV_OPKG=y \
	UCIPROV_SERVICES=y \
	UCIPROV_SSLDEPLOY=y \
	UCIPROV_TGZ_ONLYFD=y \
	UCIPROV_SU_ONLYFD=y \
	UCIPROV_DELAY_BEFORE_REBOOT="180" \
	UCIPROV_AUTOSTART=y \
	UCIPROV_REPEAT="120" \
	UCIPROV_RECOVERY_FDFLAG="EduR00m" \
	UCIPROV_MGMT=y \
	UCIPROV_MGMT_SETROOTPW=y \
	UCIPROV_REBOOT_AFTER_CFG_APPLY=y \
	UCIPROV_SSLDEPLOY_FETCHURI="http://{hostname}:{boxid}@eduroamap.{domain}/{m}/{mac}.crt" \
	UCIPROV_SSLDEPLOY_PUSHURI="http://{hostname}:{boxid}@eduroamap.{domain}/{m}/{mac}.csr"

UCIPROV_UCIDEFAULTS += system.@system[0].hostname=EduroamAp
UCIPROV_SERVICES += dropbear:enable lldpd:disable
UCIPROV_OPKG +=  "update" "install uciprov"
