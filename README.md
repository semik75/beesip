# BEESIP project - Bright Efficient Embedded SIP solution for embedded telephony

## Description

This work has been supported by the CESNET and the Ministry of Education of the Czech Republic within the project LM2010005.

The BEESIP project is based on OpenWrt. It consists of several parts

* Build system
* Target files which specifies how to construct an OpenWrt image with BEESIP feature set
* The internal applications embedded into the system which can be built by Beesip Build system

## BEESIP Build System

This is the README for build system for BEESIP.

## Introduction

Our build system consists of a set of scripts and Makefiles for automated and simplified building OpenWrt with a specific set of scripts and packages for specific target devices.

## Usage

The autobuild script makes everything you need to build an image of BEESIP. There are several actions that could be passed to this script which determines what you could do with specific target.

Simply running ./autobuild.sh script in specific branch you get the list of commands you can do and a list of targets you can build (platforms you can build for). If you do not find any target which would support your device, you can use existing targets as an inspiration for the creation of new one.

For example you can build x86 Beesip image by invoking a command:

	./autobuild.sh clean,prepare,config,build beesip_pbx-x86-cc

You can also call several commands separated by a comma in one build set to one target.

If you need to create your own target, you can take as an example existing targets. Target file is a simple .mk file stored in target directory in specific branch which consists of a set of variables that tells our build system what arch, target and packages has to be built.

## BEESIP package

Beesip package is used to glue all needed VoIP software together. It has some common operating modes. One of them is SBC. You can find detailed description [here](https://bitbucket.org/liptel/beesip/src/master/packages/net/beesip/readme.md?at=master).