USE_REFRESH=1

x86_get_rootfs() {
	local rootfsdev
	local rootfstype
	
	rootfstype="$(awk 'BEGIN { RS=" "; FS="="; } ($1 == "rootfstype") { print $2 }' < /proc/cmdline)"
	case "$rootfstype" in
		squashfs|jffs2)
			rootfsdev="$(awk 'BEGIN { RS=" "; FS="="; } ($1 == "block2mtd.block2mtd") { print substr($2,1,index($2, ",")-1) }' < /proc/cmdline)";;
		ext4)
			rootfsdev="$(awk 'BEGIN { RS=" "; FS="="; } ($1 == "root") { print $2 }' < /proc/cmdline)";;
	esac
		
	echo "$rootfstype:$rootfsdev"
}


x86_get_bootfs() {
	local bootfsdev
	local bootfstype
	
	bootfstype="ext2"
	bootfsdev="/dev/sda1"
	
	echo "$bootfstype:$bootfsdev"
}

platform_check_image() {
	[ "$ARGC" -gt 1 ] && return 1

	case "$(get_magic_word "$1")" in
		6269) return 0;;
		*)
			echo "Invalid image type"
			return 1
		;;
	esac
}

platform_refresh_partitions() {
	return 0
}

platform_copy_config() {
	local rootfs="$(x86_get_rootfs)"
	local rootfsdev="${rootfs##*:}"
	
	mount -t ext4 -o rw,noatime "${rootfsdev%[0-9]}1" /mnt
	cp -af "$CONF_TAR" /mnt/
	umount /mnt
}

platform_do_upgrade() {
	local rootfs="$(x86_get_rootfs)"
	local rootfsdev="${rootfs##*:}"
	local bootfs="$(x86_get_bootfs)"
	local bootfsdev="${bootfs##*:}"
	
	sync
	export rootfsdev bootfsdev
	[ -b ${rootfsdev%[0-9]} ] && get_image "$@" | (
	  mount -o rw,noatime "${bootfsdev%[0-9]}1" /mnt
	  mv /mnt/boot/vmlinuz /mnt/boot/vmlinuz.old
	  dd bs=1 skip=2 | (
	    zcat >/mnt/boot/vmlinuz
	    umount /mnt
	    zcat | dd of=${rootfsdev%[0-9]} bs=4096 conv=fsync
	  )
	)
	sleep 1
}
