#!/bin/bash

FILE1=streams.log
PID=_pid.txt

STATUS=`cat _status.log`

while [ $STATUS -gt 0 ];
  do
    tshark -a duration:600 -qp -z rtp,streams >> $FILE1 &
    echo $! > $PID
    sleep 600
    STATUS=`cat _status.log`
    COUNTER=`cat _counter.log`
    if [ $COUNTER -gt 1 ]; 
    then
      sh _perform.sh
      echo "0" > _counter.log
    else
      echo $(($COUNTER+1)) > _counter.log
    fi
  done