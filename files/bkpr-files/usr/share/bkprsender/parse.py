#!/usr/bin/python
#
#   TCPdump parser
#   v 0.1 for 0.0.8+
#
import re,sys

if (len(sys.argv) <= 2):
    print "No args: pass text file for parsing"
    print "use: parser.py textfile <host IP>"
    exit(1)

#print sys.argv[1]
srctext = open(sys.argv[1], "r")
#regex  for fields
rgx_ip = re.compile('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
#rgx_port = re.compile('(?\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\.)\d{1,5}')
rgx_port = re.compile('\d{1,5}(?=\s\>\s)')
rgx_method = re.compile('(OPTIONS|INVITE|REGISTER|ACK|BYE|CANCEL|SUBSCRIBE)')
rgx_datetime = re.compile('\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}')
rgx_protocol = re.compile('(?<=, proto )[UDP|TCP|TLS]{3}')

def scanFile(srctext):
    i = iter(srctext)
    line = i.next()
    try:
        while (1):
    #        print "LINE: " + str(line)
            date = regexMatch(rgx_datetime, line)
            while (date != None):
   #             print "Date: " + str(date)
                protocol = regexMatch(rgx_protocol, line)
  #              print "Protocol: " + str(protocol)
                line = i.next()
                ip = regexMatch(rgx_ip,line)
                port = regexMatch(rgx_port,line)
#                print "Port: " + str(port)
#                print "IP: " + str(ip)
                line = i.next()
                method = regexMatch(rgx_method, line)
     #           print "SIP method: " + str(method)
                line = i.next()
                #TADY je hotovo, uloz
                prepareCSV(date,protocol,ip,port,method)
                date = regexMatch(rgx_datetime, line)
                #Continue
            line = i.next()
    except StopIteration:
        return

#        print "Hotovo"

def prepareCSV(date,protocol,ip,port,method):
    if (ip != sys.argv[2]):
        print str(date) + "," + str(protocol) + "," + str(ip) + "," + str(port) + "," + str(method)

def regexMatch(regex, line):
#    print "Analyzing " + line
    m = regex.search(line)
    if (m == None):
        return None
    else:
        return m.group(0)

scanFile(srctext)

#regexMatch(rgx_protocol, line)

#for line in srctext:
#    print "Parsed: " + str(regexMatch(rgx_datetime,line))
#    exit(1)
