#
# logging function
#

blog() {
        logger -s -t "[Beesip]" $*
}

printMacAddress(){
    macAddr=$(cat /sys/class/net/$1/address)
    echo -ne $macAddr
}

printIP(){
    ip=$(ifconfig $1| grep -o "inet addr:[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*"| grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*")
    echo -ne $ip
}

printNetInterfaces(){
    ifaces=$(ls /sys/class/net/)
    echo -ne $ifaces
}

printRootfsUsage(){
    echo -ne $(df -h |grep rootfs|grep -o "[0-9]*%")
}

printFreeMemory(){
    echo -ne $(free | awk '/^Mem:/{print $3}')
}

printAvailableMemory(){
    echo -ne $(free | awk '/^Mem:/{print $2}')
}

printProcessorLoad(){
    echo -ne $(uptime |awk -F'average:' '{ print $2}')
}

sysinfo(){
    echo -e "  System information as of "$(date)"\n"
    ifaces=$(printNetInterfaces)

    for i in $ifaces
    do
            echo -e "  Interface "$i":"
            echo -e "\t\t\t"$(printIP $i)
            echo -e "\t\t\t"$(printMacAddress $i)
    done

    echo -e "  Rootfs usage:\t\t"$(printRootfsUsage)
    echo -e "  Memory usage:\t\t"$(printFreeMemory) "KBytes used of" $(printAvailableMemory) "KBytes available"
    echo -e "  Processor load: \t"$(printProcessorLoad)"\n"

}

sysinfo
